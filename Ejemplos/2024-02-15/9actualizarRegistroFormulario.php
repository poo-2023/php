<?php

$parametros = require_once "parametros.php";

require_once "funciones.php";

// desactivar errores
controlErrores();

$salida = "";
$accion = "Actualizar";

// conexion a base de datos
$conexion = @new mysqli(
    $parametros["servidor"],
    $parametros["usuario"],
    $parametros["password"],
    $parametros["nombreBd"]
);

// compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}

// compruebo si he pulsado el boton de actualizar
// y tengo que actualizar los datos del empleado
// con lo que ha escrito en el formulario
if ($_POST) {
    // leer todos los datos del formulario
    $datos["id"] = $_POST["id"];
    $datos["nombre"] = $_POST["nombre"];
    $datos["apellidos"] = $_POST["apellidos"];
    $datos["edad"] = $_POST["edad"];
    $datos["poblacion"] = $_POST["poblacion"];
    $datos["codigoPostal"] = $_POST["codigoPostal"];
    $datos["fechaNacimiento"] = $_POST["fechaNacimiento"];

    $sql = "UPDATE empleados e 
        SET 
            nombre = '{$datos["nombre"]}', 
            apellidos = '" . $datos["apellidos"] . "', 
            edad = " . $datos["edad"] . ", 
            poblacion = '" . $datos["poblacion"] . "', 
            codigoPostal = '" . $datos["codigoPostal"] . "', 
            fechaNacimiento = '" . $datos["fechaNacimiento"] . "'
            WHERE id = {$datos["id"]}";

    if ($conexion->query($sql)) {
        $salida = "Registro actualizado correctamente";
        $salida .= "<br><a href='008-listarRegistrosUpdateDelete.php'>Volver a mostrar todos los registros</a>";
    } else {
        $salida = "Error al actualizar el registro: " . $conexion->error;
    }
}

// si vengo del listado y quiere mostrar el formulario 
// con los datos del empleado a actualizar
if (isset($_GET["id"])) {
    // preparo el texto de la consulta
    $sql = "select * from empleados where id = " . $_GET["id"];

    // ejecuto la consulta
    $resultados = $conexion->query($sql);

    // saco los datos del registro a modificar
    $datos = $resultados->fetch_assoc();
}

$conexion->close();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $parametros["nombreAplicacion"] ?></title>
</head>

<body>
    <h1><?= $parametros["nombreAplicacion"] ?></h1>
    <div>
        <?= $salida ?>
    </div>
    <br>
    <div>
        <?php require "_form.php" ?>
    </div>

</body>

</html>
