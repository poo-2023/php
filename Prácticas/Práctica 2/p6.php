<?php

// opción 1
// no es válido para el interior de clases
// define("NOMBRE","FRAN");

// opción 2
// es válido siempre
const NOMBRE="FRAN";

echo NOMBRE;

