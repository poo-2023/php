<?php
// formulario que permita meter un número 
// que muestre todos los números introducidos
// hacerlo con sesiones

session_start();
$numero=[];

if(($_POST)){
    $numero=$_SESSION["numero"] ?: [];
    array_push($numero, $_POST["numero"]);
    $_SESSION["numero"]=$numero;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="post">
        <div>
            <label for="numero" name="numero">Numero</label>
            <input type="number" name="numero" id="numero">
        </div>
        <div>
            <button>Enviar</button>
        </div>
        <?= var_dump($_SESSION) ?>
    </form>
</body>
</html>