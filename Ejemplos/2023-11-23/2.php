<?php   

// crear variable de tipo texto
$numero = 3;

// crear variable de tipo numero
$texto = "variable de tipo texto";

// crear variable de tipo booleano
$bool = true;

// depuración de las variables
var_dump($texto, $numero, $bool);

