<?php
// cargar la libreria 001-arrays.php
require_once '001-arrays.php';

// cargar los datos que estan en datos.php
require_once 'datos.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Formas de recorrer el array</h1>
    <h2>FOR</h2>
    <?= recorrer($ciudades) ?>
    <?= recorrer($edades) ?>
    <h2>FOR CON ARRAY_VALUES</h2>
    <?= recorrerV1($ciudades) ?>
    <?= recorrerV1($edades) ?>
    <?= recorrerV1($valores) ?>
    <?= recorrerV1($colores) ?>
    <h2>FOREACH</h2>
    <?= recorrer2($ciudades) ?>
    <?= recorrer2($edades) ?>
    <?= recorrer2($valores) ?>
    <?= recorrer2($colores) ?>
    <h2>while con array_values</h2>
    <?= recorrer3($ciudades) ?>
    <?= recorrer3($edades) ?>
    <?= recorrer3($valores) ?>
    <?= recorrer3($colores) ?>
    <h2>array_walk</h2>
    <?= recorrer4($ciudades) ?>
    <?= recorrer4($edades) ?>
    <?= recorrer4($valores) ?>
    <?= recorrer4($colores) ?>
    <h2>array_walk con implode</h2>
    <?= recorrer4V1($ciudades) ?>
    <?= recorrer4V1($edades) ?>
    <?= recorrer4V1($valores) ?>
    <?= recorrer4V1($colores) ?>
    <h2>array_map con implode</h2>
    <?= recorrer5($ciudades) ?>
    <?= recorrer5($edades) ?>
    <?= recorrer5($valores) ?>
    <?= recorrer5($colores) ?>



</body>

</html>