<?php

//  funcion llamada mostrarArray que recorre un array pasado como 
// primer parametro y retorna una tabla HTML
// donde en la primera columna esta el indice y en la segunda el valor
// el indice debe estar como th y el valor como td
// utilizar para recorrer el array for
// solo admite arrays unidimensionales de tipo enumerado y secuenciales

function mostrarArray(array $datos): string
{
    $html = "<table>";

    return $html;
}

?>

<table>
    <tr>
        <th>RED</th>
        <td>Rojo</td>
    </tr>
    <tr>
        <th>Blue</th>
        <td>Azul</td>
    </tr>
</table>
