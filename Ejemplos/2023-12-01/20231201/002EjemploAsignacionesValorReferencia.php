<?php

$numero1 = 0;

$numero2 = $numero1;

$numero3 = &$numero1;
// lo que le pase a numero 1 le pasa a numero 3, y viceversa
// se hacen dos variables que apuntan al mismo sitio

$numero1 = 45;

// cuanto valen las variables
// numero1 45
// numero2 0
// numero3 45
echo "numero1 vale " . $numero1 . "<br>";
echo "numero2 vale " . $numero2 . "<br>";
echo "numero3 vale " . $numero3 . "<br>";

// realizar la misma impresion sin utilizar concatenar
// directamente con las comillas

echo "numero1 vale {$numero1}<br>";
echo "numero2 vale {$numero2}<br>";
echo "numero3 vale {$numero3}<br>";

// realizar la misma impresion pero con el modo contraido de impresion
?>

numero1 vale <?= $numero1 ?><br>
numero2 vale <?= $numero2 ?><br>
numero3 vale <?= $numero3 ?><br>