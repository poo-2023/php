<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Método 1</h2>
    <p>
        <?php
        // mezclamos html y php
        echo "<p> Hola mundo<p>";
        ?>
    </p>

    <h2>Método 2</h2>
    <p>
        <?php
        // Solo coloco php
        echo "Hola mundo";
        ?>
    </p>

    <h2>Metodo 3</h2>
    <?php
        echo "<p>";
    ?>
    Hola mundo
    <?php
        echo "</p>";
    ?>

    <h2>Podemos utilizar el modo contraído del echo</h2>

    <p>
        <?= "hola mundo" ?>
    </p>
</body>
</html>