<?php

function mostrar_formulario($errores, $inicio="", $fin=""){
    if(count($errores)){
        echo "<div>";
        foreach ($errores as $error) {
            echo "error <br>";
        }
        echo "</div>";
    }
}

function mostrar_resultado($d){
    echo "<div>";
    echo $d;
    echo "</div>";
}

$errores = [];

if($_GET){
    $inicio=$_GET["fechai"];
    $final=$_GET["fechaf"];

    $inicioArray=explode("/", $inicio);
    if(!@checkdate($inicioArray[1], $inicioArray[0], $incioArray[2])){
        $errores[]="La fecha de inicio no es correcta";
    }

    $finArray=explode("/", $fin);
    if(!checkdate($finArray[1], $finArray[0], $finArray[2])){
        $errores[]="La fecha de fin no es correcta";
    }

    if (!count($errores)){
        $i=strtotime(implode("/", array_reverse($inicioArray)));
        $f=strtotime(implode("/", array_reverse($finArray)));
        if($i<=$f){
            $d=floor(abs(($f-$i)/(24*60*60)));
            mostrar_resultado($d);
        }else{
            $errores[]="La fecha de fin debe ser mayor que la de inicio";
        }
    }
    mostrar_resultado($d);
}else{
    mostrar_formulario($errores);
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="get">
        <label for="fechai">Introduce fecha inicial</label>
        <input type="date" id="fechai" name="fechai" required="true" placeholder="dd/mm/aaaa"<?= $inicio?> />
        <label for="fechaf">Introduce fecha final</label>
        <input type="date" id="fechaf" name="fechaf" required="true" placeholder="dd/mm/aaaa"<?= $fin?> />
        <input type="submit" value="Calcular">
    </form>
</body>
</html>