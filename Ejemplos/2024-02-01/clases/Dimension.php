<?php
namespace clases;

class Dimension{
    private float $alto;
    private float $ancho;
    private float $profundo;


    public function __construct(){
        $this->alto = 0;
        $this->ancho = 0;
        $this->profundo = 0;

    }

    // getters and setters

    public function getAlto(){
        return $this->alto;
    }

    public function getAncho(){
        return $this->ancho;
    }

    public function getProfundo(){
        return $this->profundo;
    }

    public function setAlto(float $alto){
        $this->alto = $alto;

        return $this;
    }

    public function setAncho(float $ancho){
        $this->ancho = $ancho;

        return $this;
    }

    public function setProfundo(float $profundo){
        $this->profundo = $profundo;
        
        return $this;
    }

    public function getVolumen(){
        return $this->alto * $this->ancho * $this->profundo;
    }

    public function __toString(){
        $salida = "Alto: " . $this->alto . ", Ancho: " . $this->ancho . 
        ", Profundo: " . $this->profundo . ", Volumen: " . $this->getVolumen();

        return $salida;
    }
}
