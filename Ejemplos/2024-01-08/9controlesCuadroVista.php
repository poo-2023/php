<?php
// inicializar variables

$resultado="";

if($_POST){
    $resultado=implode(",", $_POST["opciones"]);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="post">
        <select name="opciones[]" multiple>
            <option>Coche</option>
            <option>Bicicleta</option>
            <option>Moto</option>
        </select>

        <button type="submit">Enviar</button>
    </form>
    <?= $resultado ?>
</body>
</html>