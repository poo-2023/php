<form method="post">
    <div class="row">
        <label class="col-sm-2 col-form-label" for="url">Url</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="url" value="<?= $datos['url'] ?>">
        </div>
    </div>
    <br><br>
    <div class="row">
        <label for="titulo" class="col-sm-2 col-form-label">Titulo</label>
        <div class="col-sm-10">
            <input type="number" class="form-control" name="titulo" value="<?= $datos['titulo'] ?>">
        </div>
    </div>
    <br><br>
    <div class="row">
        <label for="descripcion" class="col-sm-2 col-form-label">Descripcion</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="descripcion" value="<?= $datos['descripcion'] ?>">
        </div>
    </div>
    <br><br>    
    <div class="row">
        <label for="categorias" class="col-sm-2 col-form-label">Categorias</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="categorias" value="<?= $datos['categorias'] ?>">
        </div>
    </div>
    <br><br>
    <div>
        <button class="btn btn-primary"><?= $accion ?></button>
        <button class="btn btn-secondary" type="reset">Limpiar</button>
    </div>
</form>
