# APLICACIÓN PARA CREAR TABLÓN DE ANUNCIOS

El usuario, cuando entra a la aplicación debe colocar su nombre.

Y, una vez que ha colocado el nombre, le debe permitir añadir mensajes.

A medida que añades mensajes, debes ver los mensajes tuyos (con tu nombre),
y los mensajes que hayan colocado otros usuarios (no automáticamente).

#PÁGINA INDEX:

-Debe salir el nombre de usuario y el botón de entrar
-Una vez logueado debe poner el menú y "Bienvenido" y su nombre

#PÁGINA MENSAJES:

-Que permita escribir mensajes
-En la parte inferior debe listar todos los mensajes que hay en el sistema
-Además se tiene que colocar un botón que permita borrar cualquier mensaje del sistema

Cuando pulse eliminar un mensaje me debería levar a una página llamada eliminar 
que borrase ese emnsaje y me redireccione a la página mensaje de nuevo

#MENÚ

-En todas las páginas debe colocar un menú 

-Inicio (página de index)
-Mensaje (página de mensaje)
-Salir (logout)

#CONSIDERACIONES

Cuando el usuario no esté logueado no puede ver
el menú y no debe poder entrar a ninguna página excepto
la del index para loguear

#AYUDA

Sería recomendable una tabla para los usuarios y otra para los mensajes
(id, nombre, mensaje, fechaHora)

Necesitamos las variables de sesión para gestionar en login

