<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
<?php

$alumnos=array();
$alumnos[]= "Ramon";
$alumnos[]= "Jose";
$alumnos[]= "Pepe";
$alumnos[]= "Ana";

$alumnos1=array("Ramon", "Jose", "Pepe", "Ana");

for ($c= 0;$c<count($alumnos);$c++) {
    echo "$alumnos[$c]<br>";
}

foreach ($alumnos1 as $value) {
    echo "$value<br>";
}

// el primer bucle y el segundo dan la misma salida. 
// El primero usa un contador para calcular cuantas veces tiene que emitir la instrucción, y el segundo la ejecuta tantas veces como variables tenga el array
?>


</body>
</html>