<?php

// lógica de control

$operacion=$_POST["operacion"] ?? "";
$numero1=$_POST["numero1"] ?? 0;
$numero2=$_POST["numero2"] ?? 0;


switch ($operacion) {
    case 'sumar':
        $resultado=$numero1+$numero2;
        break;
    case 'restar':
        $resultado=$numero1-$numero2;
        break;
    default:
        $resultado=0;
        break;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="post">
        <div>
            <label for="numero1">Número 1</label>
            <input type="number" name="numero1" id="numero1" required>
        </div>
        <div>
            <label for="numero2">Número 2</label>
            <input type="number" name="numero2" id="numero2" required>
        </div>
        <div>
            <button name="operacion" value="sumar">Sumar</button>
            <button name="operacion" value="restar">Restar</button>
        </div>
    </form>

    <?= $resultado?>
</body>
</html>