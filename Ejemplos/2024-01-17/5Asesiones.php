<?php




session_start();

$numeros=$_SESSION["numeros"] ?? [1,2,3,4,5,6,7,8,9,10];
$aciertos=$_SESSION["aciertos"] ?? 0;
$errores=$_SESSION["errores"] ?? 0;
$intentos=$_SESSION["intentos"] ?? 0;

if(isset($_POST["numero"])){
    $numero=$_POST["numero"];

    if(in_array($numero, $numeros)){
        $aciertos[array_search($numero, $numeros)]=$numero;
        unset($numeros[array_search($numero, $numeros)]);
    }else {
        $errores++;
    }

    $intentos++;

    if(count($numeros)==0){
        $final=1;
    }
}

$_SESSION["aciertos"] = $aciertos; // numeros que he acertado
$_SESSION["errores"] = $errores; // numero de errores cometidos
$_SESSION["intentos"] = $intentos; // numero de intentos
$_SESSION["numeros"] = $numeros; // numeros que me quedan por acertar

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <form method="post">
        <div>
            <label for="numero" name="numero">Numero</label>
            <input type="number" name="numero" id="numero">
        </div>
        <div>
            <button type="submit">Jugar</button>
        </div>
        <div>
            <?= "Número de intentos: " .  $_SESSION["intentos"] ?>
            <?= "Errores: " . $_SESSION["errores"] ?>
            <?= "Aciertos: " . $_SESSION["aciertos"] ?>
        </div>
    </form>

</body>
</html>