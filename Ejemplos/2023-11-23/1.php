<?php

// creo variable y asigno valor
$texto="ejemplo de clase";
$a= 10;
$b= 2.34;
$c= true;
$d= null;
$e= "";
$f= ""; //las variables en php no inicializadas se consideran error

// mostrando la variable
echo $texto;

// depurar la variable (inspección de valor y tipo)
var_dump($texto);

//  depurar todas las variables
var_dump($a, $b, $c, $d, $e, $f);  

