<?php

$nombre = "Francisco";
$edad = 31;
$poblacion = "Santander";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <table border="1" width="80%" align="center">
        <tr>
            <td>Nombre</td>
            <td> <?= $nombre ?></td>
        </tr>
        <tr>
            <td>Edad</td>
            <td><?= $edad ?></td>
        </tr>    
        <tr>
            <td>Población</td>
            <td><?= $poblacion ?></td>
        </tr>
    </table>
    
</body>
</html>