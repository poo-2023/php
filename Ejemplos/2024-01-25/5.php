<?php

// importa la clase al espacio de nombres actual
use clases\fran\Texto;

spl_autoload_register(function($clase){
    include $clase . ".php";
});

$objeto=new Texto();

echo $objeto->mostrar();