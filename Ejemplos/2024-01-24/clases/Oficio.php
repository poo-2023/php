<?php

class Oficio{
    public $nombre;
    public $salarioBase;
    public $horasSemanales;

    public function calcular(){
        return $this->salarioBase * $this->horasSemanales;
    }

    public function __construct($nombre="", $salarioBase=0, $horasSemanales=0){
        $this->nombre=$nombre;
        $this->horasSemanales=$horasSemanales;
        $this->salarioBase=$salarioBase;
    }
}