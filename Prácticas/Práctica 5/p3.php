<?php

$a=190;
$b=[
    "poco",
    "algo",
    "medio",
    "mucho",
    "enorme",
];

if($a<10){
    echo $b[0];
}elseif($a<20){
    echo $b[1];
}elseif($a<30){
    echo $b[2];
}elseif($a<40){
    echo $b[3];
}else{
    echo $b[4];
}

// el bucle, $siendo la variable $190, marcará $b[4]="enorme". 
// Para que marque el resto, se podría poner la variable $a a 1,15,21 y 48, por ejemplo

