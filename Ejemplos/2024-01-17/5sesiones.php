<?php

    // se trata de hacer un juego en el que el usuario trata de adivinar los números
    // creamos un formulario en el que se escribe un número y al pulsarlse enviar se envía
    // si está, se indica que se ha acertado y lo muestra
    // sino, se cuenta un fallo y se debe mostrar la cantidad de fallos
    // cuando se adivinen los 10 números, la puntuación será el número de intentos
    // eliminar las variables de sesión una vez se han adivinado los 10 números
    // que lso números sean aleatorios 1-50 en lugar de introducirlos a mano
    
session_start();

// variable sesión números que tedrá los números que quedan por acertar
// si la variable no existe, se crea y se meten los números
$numeros=$_SESSION["numeros"] ?? [1,2,3,4,5,6,7,8,9,10];

// se crea una veriable de sesión por cada variable a usar
// aciertos almacena cada número que se acierta en la posición del array original
// aciertos tiene los números o guiones
$aciertos=$_SESSION["aciertos"] ?? array_fill(0, count($numeros), "-");

// fallos guarda los fallos y si no hay erorres inicializa a 0
$fallos=$_SESSION["fallos"] ?? 0;

// intentos cuenta los intentos e inicializa a 0
$intentos=$_SESSION["intentos"] ?? 0;

// variable que controla si se sigue jugando
// cuando sea 1, se reinicia el código
$final=0;

// comprobar que se ha pulsado el botón de enviar
if(isset($_POST["jugar"])){

}

// unset es para eliminar variables, y usada en un array elimina un elemento, pero manteniendo los índices originales

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <form method="post">
        <div>
            <label for="numero" name="numero">Numero</label>
            <input type="number" name="numero" id="numero">
        </div>
        <div>
            <button type="submit"></button>
        </div>
        <?= "Número de intentos: " .  $_SESSION["intentos"]?>
        <?= "Fallos: " . $fallos ?>
        <?= "Aciertos: " . $aciertos ?>
    </form>

</body>
</html>