<?php

// crear objeto clase texto de fran y otro de profesor
// llamar al metodo mostrar de cada uno de ellos
spl_autoload_register(function($clase){
    include $clase . ".php";
});


$textoProfesor=new \clases\profesor\Texto();
$textoFran=new \clases\fran\Texto();

echo $textoFran->mostrar();
echo $textoProfesor->mostrar();