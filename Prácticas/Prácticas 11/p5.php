<?php


// esta funcion tiene de argumentos un array, cuyos elementos se analizaran y una opción para devolver todos o no
// si hay elementos repetidos en el array arguento, se genera un nuevo array en el que se meterán los elementos repetidos
// el primer foreach crea un booleano falso y crea otro foreach que si hay un valor que sea identico a otro da instrucciones en un if
// si es falso toma un elemento del array y asigna un valor al contador $i

function elementosRepetidos($array, $devolverTodos = false) {

    $repeated=array();

    foreach((array) $array as $value) {
        $inArray=false;

        foreach($repeated as $i => $rItem) {
            if($rItem['value']===$value){
                $inArray=true;
                ++$repeated[$i]['count'];
            }
        }

        if(false === $inArray) {
            $i=count($repeated);
            $repeated[$i]=array();
            $repeated[$i]['value']=$value;
            $repeated[$i]['count']=$i;
        }


    }

    if  (!$devolverTodos){
        foreach($repeated as $i => $rItem) {
            if($rItem['count']===$i){
                unset($repeated[$i]);
            }
        }
    }

sort ($repeated);

return $repeated;

}