<?php

namespace src;

class Persona{
    private ?string $nombre;

    private ?int $edad;

    // getters y setters

    public function getNombre():string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre):void
    {
        $this->nombre=$nombre;
    }

    public function getEdad():int
    {
        return $this->edad;
    }

    public function setEdad(int $edad):void 
    {
        $this->edad=$edad;
    }

    public function mostrar():string
    {
        $salida="<ul>";
        $salida.="<li>Nombre: $this->nombre</li>";
        $salida.="<li>Edad: $this->edad</li>";
        $salida.="</ul>";
        return $salida;
    }

    public function __toString()
    {
        return $this->mostrar();
    }

    public function __construct()
    {
        $this->nombre=null;
        $this->edad=null;
    }
}