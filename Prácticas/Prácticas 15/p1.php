<?php

function mediaNotas($a, $b, $c){
    $media=($a+$b+$c)/3;

    if($media < 60){
        $resultado = "F";
    }else if($media < 70){
        $resultado = "D";
    }else if($media < 80){
        $resultado = "C";
    }else if($media < 90){
        $resultado = "B";
    }else if($media <= 100){
        $resultado= "A";
    }

    return $resultado;
}


echo mediaNotas(50,60,80);