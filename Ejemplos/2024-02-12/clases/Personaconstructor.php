<?php

namespace clases;

class Personaconstructor
{
    public int $id;
    public string $nombre;
    public string $apellidos;
    public int $edad;
    public string $fechaNacimiento;

    public string $poblacion;

    public string $codigoPostal;

    public function __toString()
    {
        $salida = "<ul>";
        foreach ($this as $campo => $valor) {
            $salida .= "<li>{$campo} : {$valor}</li>";
        }
        $salida .= "</ul>";
        return $salida;
    }

    public function __construct(array $datos = [])
    {
        foreach ($datos as $campo => $valor) {
            $this->{$campo} = $valor;
        }
        // $this->id = $datos["id"];
        // $this->nombre = $datos["nombre"];
        // ....
    }
}



