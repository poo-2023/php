<?php
// definiendo el autoload

use clases\Aplicacion;
use clases\Pagina;
use clases\Header;
use clases\Modelo;


spl_autoload_register(function ($clase) {
    include $clase . '.php';
});

$aplicacion = new Aplicacion();
$favoritos = new Modelo($aplicacion->db);
$favoritos->query("select * from favoritos where categorias = 'formacion'");


$cabecera = Header::ejecutar([
    'titulo' => 'Formacion',
    'subtitulo' => "",
    'salida' => 'Paginas de formacion'
]);

Pagina::comenzar();
?>
    <?= $favoritos->gridViewBotones(); ?>
<?php
Pagina::terminar([
    "titulo" => "formacion",
    "cabecera" => $cabecera,
    "pie" => "Creado por: " . $aplicacion->configuraciones['autor']
]);
