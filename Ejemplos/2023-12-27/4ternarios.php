<?php

$datos=[
    "nombre"=>"Pepe",
    
];

// quiero realizarlo con el operador ternario recortado 

// echo $datos["nombre"]="Pepe" ?? "no hay datos";
// echo $datos["apellido"]="no conocido" ?? "no hay datos";
// echo $datos["edad"]="no conocida" ?? "no hay datos";

// operador recortado elvis

echo $datos["nombre"] ?? "no hay datos";
echo $datos["apellido"] ?? "no hay datos";
echo $datos["edad"] ?? "no hay datos";