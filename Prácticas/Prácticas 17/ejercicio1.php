<?php

use clases\ejercicio1\Electrodomestico;

spl_autoload_register(function ($clase) {
    include $clase . '.php';
});

$objeto=new Electrodomestico("Nevera", "LG", 200);

echo $objeto->getCosteConsumo(12,0.4);

