<?php

// // crear una función que reciba 
// como primer argumento una etiqueta html 
// como segundo argumento un contenido de la etiqueta
// debe devolver una etiquta html con el contenido

function crearEtiqueta(string $etiqueta, string $contenido):string
{
    return "<{$etiqueta}>{$contenido}</{$etiqueta}>";
}

echo crearEtiqueta("p", "Hola mundo");