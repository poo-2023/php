<?php


// esta función tiene como parámetros un mínimo, otro máximo y otro numer
// mínimo y máximo serán los topes entre los que se buscarán los números aleatorios con mt_rand
// número será la longitud del array
// bucle for mete un número aleatorio tantas veces como el número que se haya establecido como longitud del array
// devuelve el array 

function ejercicio($minimo, $maximo, $numero){

    $vector=[];

    for ($i=0; $i < $numero; $i++ ) { 
        $vector[$i]=mt_rand($minimo, $maximo);
    }

    return $vector;
}

