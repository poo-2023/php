<?php

use clases\Dimension;

spl_autoload_register(function($clase){
    include $clase . '.php';
});

$objeto=new Dimension();

$objeto
    ->setAncho(10)
    ->setAlto(20)
    ->setProfundo(30);

echo $objeto;

