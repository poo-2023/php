<?php
use clases\Personaconstructor;

require_once "funciones.php";
require_once "autoload.php";

// objeto de tipo mysqli
$conexion = new mysqli("localhost", "root", "", "personas1");

//objeto de tipo mysqli_result
$resultados = $conexion->query("select * from personas");

// array asociativo con el primer registro
$registro = $resultados->fetch_assoc();

// leer el nombre
echo $registro["nombre"] . "<br>";

var_dump($registro);

// objeto de tipo stdClass con el segundo segundo registro
$registro = $resultados->fetch_object();

// leer el nombre
echo $registro->nombre . "<br>";

var_dump($registro);

// echo $registro; // produce error porque no tiene toString

// puedo con fetch_object crear un objeto de tipo Persona

$registro = $resultados->fetch_object("clases\Persona");

// leer el nombre
echo $registro->nombre . "<br>";

var_dump($registro);

echo $registro;

// quiero utilizar fetch_object para crear un objeto de
// de tipo Personaconstructor
// no puedo porque tiene constructor y crearía un objeto vacío
// para solucionar el problema se crea un constructor especial

// opción 1
$registro = $resultados->fetch_object("clases\Personaconstructor");

// opción 2
$registro = new Personaconstructor($resultados->fetch_assoc());

var_dump($registro);