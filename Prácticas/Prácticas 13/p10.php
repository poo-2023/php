<?php

function sumaArrays(array $vector1, array $vector2, array $vector3){
    $suma=0;

    foreach ($vector1 as $value) {
        $suma=$suma+$value;
    }

    foreach ($vector2 as $value) {
        $suma=$suma+$value;
    }

    foreach ($vector3 as $value) {
        $suma=$suma+$value;
    }

    echo $suma;
};

$vector1=[1,2,3,4];
$vector2=[2,3,4,5];
$vector3=[3,4,5,6];

sumaArrays($vector1, $vector2, $vector3);