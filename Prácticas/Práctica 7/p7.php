<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
<?php

$alumnos=[     
    "Ramon",
    "Jose",
    "Pepe",
    "Ana",
];

echo $alumnos[0];
echo $alumnos[1];
echo "<br>";
echo "$alumnos[2]";
echo "<br>";
echo $alumnos[3];
?>

<div>

    <?php
        echo "$alumnos[0]<br>$alumnos[1]<br>$alumnos[2]<br>$alumnos[3]";
    ?>

</div>

<!--  aparecen todos los nombres en una línea independiente, excepto los 2 primeros que están en la misma   -->
</body>
</html>
