<?php

class Coche{

    private string $color;
    private int $numeroPuertas;
    private string $marca;
    private int $gasolina;

    public function __construct($color="", $numeroPuertas=0, $marca="", $gasolina=0){
        $this->color=$color;
        $this->numeroPuertas=$numeroPuertas;
        $this->marca=$marca;
        $this->gasolina=$gasolina;
    }
    
    public function llenarTanque(int $gasolinaNueva): void{
        $this->gasolina+=$gasolinaNueva;
    }

    public function acelerar(int $gasolina){
        if($gasolina>0){
            $gasolina=$gasolina-1;
        }

        return ;
    }

    // colocar getters y setters

    
    public function getGasolina(): int
    {
        return $this->gasolina;
    }

    public function setGasolina(int $gasolina): void
    {
        $this->gasolina=$gasolina;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function setColor(string $color): void
    {
        $this->color=$color; 
    }
    
    public function getNumeroPuertas(): int
    {
        return $this->numeroPuertas;
    }

    public function setNumeroPuertas(int $numeroPuertas): void
    {
        $this->numeroPuertas=$numeroPuertas;
    }

    public function getMarca(): string  
    {
        return $this->marca;
    }

    public function setMarca(string $marca): void
    {
        $this->marca=$marca;
    }
}