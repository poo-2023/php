<?php
use clases\ejercicio1\Persona;

spl_autoload_register(function ($clase) {
    include $clase . '.php';
});

$persona1=new Persona("Juan", 30);
echo $persona1->mostrarInformacion();