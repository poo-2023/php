<?php
$dado1=mt_rand(1,6);
$dado2=mt_rand(1,6);

$total=$dado1+$dado2;

$dadoAncho=50;
$dadoAlto=50;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    

<div width="<?=$dadoAncho*2?>" height="<?=$dadoAlto?>">
    <img src="dados/<?=$dado2?>.svg" alt="" width="<?=$dadoAncho?>" height="<?=$dadoAlto?>">
    <img src="dados/<?=$dado1?>.svg" alt="" width="<?=$dadoAncho?>" height="<?=$dadoAlto?>">
</div>
<div>
    Total: <?= $total?>
</div>

</body>
</html>