<?php

namespace clases;

use mysqli;

class Aplicacion
{
    // creo unas constantes de clase
    const FILE_CONFIG = 'config/configuracion.json';
    const FILE_PARAMS = 'config/params.php';

    // aqui tendre el titulo de la aplicacion
    public string $titulo;

    // aqui tengo el autor de la aplicacion
    public string $autor;

    // aqui tendre un objeto de tipo mysqli con la
    // conexion a la base de datos
    public mysqli $db;

    // aqui tendre todas las configuraciones de mi aplicacion
    public array $configuraciones;

    // aqui tendre todos los parametros de mi aplicacion
    public array $parametros;


    private function obtenerConfiguraciones(): void
    {
        $this->configuraciones = json_decode(file_get_contents(self::FILE_CONFIG), true);
    }


    private function conectar()
    {
        $this->db = new mysqli(
            $this->configuraciones['db']['host'],
            $this->configuraciones['db']['user'],
            $this->configuraciones['db']['password'],
            $this->configuraciones['db']['database'],
            $this->configuraciones['db']['port']
        );
    }

    public function __construct()
    {
        // leer las configuraciones del JSON
        $this->obtenerConfiguraciones();

        // leer los parametros de configuracion
        $this->parametros = include(self::FILE_PARAMS);

        // leo el titulo
        $this->titulo = $this->configuraciones['titulo'];

        // leo el autor
        $this->autor = $this->configuraciones['autor'];

        // inicializo la conexion con la base de datos
        $this->conectar();
    }

    public static function menu(array $elementos): string
    {
        $salida = "<ul class='nav my-2 bg-dark text-light nav-pills justify-content-center'>";

        foreach ($elementos as $label => $enlace) {
            $salida .= "<li class='nav-item'><a class='text-custom nav-link' href='{$enlace}'>{$label}</a></li>";
        }

        $salida .= "</ul>";
        return $salida;
    }
}

