<?php

namespace clases;

use mysqli_result;
class GridView
{

    public static function render(mysqli_result $resultados, array $botones = [], array $campos = []): string
    {
        if ($resultados->num_rows > 0) {
            $registros = $resultados->fetch_all(MYSQLI_ASSOC);
            // mostrar los registros
            $salida = "<table class='table table-striped table-bordered'>";
            $salida .= "<thead class='table-dark'><tr>";

            // compruebo si el usuario ha pasado los campos a mostrar
            if(count($campos)== 0){
                //si no ha pasado los campos, los paso del primer registro de la base de datos
                $campos = array_keys($registros[0]);
            }

            foreach ($campos as $campo) {
                $salida .= "<th>$campo</th>";
            }
            if (count($botones) > 0) {
                // añado una columna para los botones
                $salida .= "<td>Acciones</td>";
            }

            $salida .= "</tr></thead>";
            // muestro todos los registros
            foreach ($registros as $registro) {
                $salida .= "<tr>";
                // mostrando los campos
                foreach ($campos as $campo) {
                    $salida .= "<td>" . $registro[$campo] . "</td>";
                }
                // mostrando los botones
                if (count($botones) > 0) {
                    $salida .= "<td>";

                    foreach ($botones as $label => $enlace) {
                        $salida .= "<a href='{$enlace}?id={$registro['id']}'>{$label}</a>";
                    }

                    // cerramos la celda
                    $salida .= "</td>";
                }

                $salida .= "</tr>";
            }
            $salida .= "</table>";
        } else {
            $salida = "No hay registros";
        }
        // colocamos el puntero de la busqueda al principio
        $resultados->data_seek(0);
        return $salida;
    }



}
