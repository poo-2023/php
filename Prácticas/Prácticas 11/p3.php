<?php

// creamos una función a la que se le pasa como argumento el número de colores a crear
// en el interior de la función se crea el array
// bucle for que mete tantos elementos como se haya pedido como argumento, e introduce #como primer caracter de cada elemento
// dentro de ese for, otro for que empieza en el 2º caracter y completa los 7 dígitos de forma aleatoria en base hexadecimal 

function color($numero){
    $arrayColores=[];

    for($i= 0;$i<$numero;$i++){

        $arrayColores[$i]="#";
        for($j= 1;$j<7;$j++){
            $arrayColores[$i]=dechex(mt_rand(0,15));
        }
    }

    return $arrayColores;
}