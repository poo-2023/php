<?php
// -- UTILIZAMOS FETCH_ALL --

// mostrar todos los datos de la tabla personas
// de la base de datos personas1
// del servidor de base de datos que esta en localhost

// Quiero que utilicemos el metodo fetch_all
// utilizando un array asociativo para cada registro

// cargo la libreria con funciones para mostrar informacion
require_once "funciones.php";
require_once "controlErrores.php";
// Definir la información de conexión a la base de datos
$host = "localhost";
$usuario = "root";
$contrasena = "";
$base_de_datos = "personas1";

// 1 paso 
// realizar la conexion
$conexion = new mysqli($host, $usuario, $contrasena, $base_de_datos);

// 2 paso 
// comprobar que la conexion se ha realizado correctamente
if ($conexion->connect_error) {
    die("La conexion ha fallado: " . $conexion->connect_error);
}

// 3 paso
// realizar la consulta
$consulta = "select * from personas";
$resultados = $conexion->query($consulta);
// me retorna un objeto de tipo mysqli_result

// 4 paso 
// colocar los resultados en pantalla
// utilizamos for y un foreach
// utilizamos for

// creo un array enumerado de arrays asociativos
$registros = $resultados->fetch_all(MYSQLI_ASSOC);
// mostrar los datos en formato tabla
echo gridViewTable($registros);

//echo gridViewTable($registros, ["id", "nombre", "apellidos", "edad"]);
// mostrarlo en formato lista
echo ListView($registros);
// 5 paso
// cerrar la conexion

$conexion->close();

