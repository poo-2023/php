<?php

namespace src;

class Directivo extends Empleado{
    private ?string $categoria;

    // sobreescribir mostrar 

    public function mostrar(): string
    {
        $salida="<ul>";
        $salida.="<li>Nombre: " . $this->getNombre() . "</li>";
        $salida.="<li>Edad: " . $this->getEdad() . "</li>";
        $salida.="<li>Categoria: " . $this->categoria . "</li>";
        $salida.="</ul>";

        return $salida;
    }
    
    // getters y setters

    public function getCategoria(): string
    {
        return $this->categoria;
    }

    public function setCategoria(string $categoria): void
    {
        $this->categoria = $categoria;
    }
}