<?php

namespace clases\ejercicio1;

class Electrodomestico
{
    public string $tipo;
    public string $marca;
    public float $potencia;

    public function __construct(string $tipo, string $marca, float $potencia){

        $this->potencia = $potencia;
        $this->marca = $marca;
        $this->tipo = $tipo;
    }

    // getters y setters

    public function getPotencia(): float
    {
        return $this->potencia;
    }

    public function setPotencia(float $potencia): void  
    {
        $this->potencia = $potencia;
    }

    public function getTipo(): string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): void 
    {
        $this->tipo = $tipo;
    }

    public function getMarca(): string
    {
        return $this->marca;
    }

    public function setMarca(string $marca): void   
    {
        $this->marca = $marca;
    }

    public function toString()
    {
        return "Tipo: " . $this->tipo . ", Marca: " . $this->marca . ", Potencia: " . $this->potencia;
    }

    public function getConsumo(int $horas)
    {
        return "El consumo es " . $this->potencia * $horas;
    }

    public function getCosteConsumo(int $horas, float $coste){
        return "El coste es " . $this->potencia * $horas * $coste;
    }

}