<?php
header("Content-type: image/png");

// variables para definir las dimensiones del lienzo
$width = 200;
$height = 200;

// creo lienzo dibujo
$imagen = imagecreatetruecolor($width, $height);

// coloco color fondo
$colorFondo = imagecolorallocatealpha($imagen, 37, 127, 67, 50);

// relleno el lienzo con el color de fondo
imagefill($imagen, 0, 0, $colorFondo);

// creo colores
$negro = imagecolorallocate($imagen, 0, 0, 0);
$rojo = imagecolorallocate($imagen, 255, 0, 0);
$verde = imagecolorallocate($imagen, 0, 255, 0);
$color1 = imagecolorallocatealpha($imagen, 0xC, 0xC, 0xC, 100);


imageline($imagen,10, 10, 100, 10, $negro);
imageline($imagen,100, 10, 10, 100, $negro);
imageline($imagen,10, 100, 10, 10, $negro);

// dibuja la imagen
imagepng($imagen);
