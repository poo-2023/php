<?php

//contador de visitas que cuando llegue a 10 se reinicie
session_start();

if(isset($_SESSION["visitas"])){
    $_SESSION["visitas"]++;
}else{
    $_SESSION["visitas"]=1;
}

echo $_SESSION["visitas"];

if( $_SESSION["visitas"]>=10){
    session_destroy(); // elimina la sesión en la próxima carga, no en la actual
}

echo $_SESSION["visitas"];