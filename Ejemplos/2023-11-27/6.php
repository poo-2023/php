<?php
// datos del alumno con la nota más alta

$datos=[
    "id"=> 12,
    "nombre"=> "Eva",
    "apellidos"=>"Gomez Palomo",
    "poblacion"=>"Laredo"
];

?>

<table>
    <tr>
        <td>Campo</td>
        <td>Valor</td>
    </tr>
    <tr>
        <td>ID</td>
        <td><?= $datos["id"]?></td>
    </tr>
    <tr>
        <td>Nombre</td>
        <td><?= $datos["nombre"]?></td>
    </tr>
    <tr>
        <td>Apellidos</td>
        <td><?= $datos["apellidos"]?></td>
    </tr>
    <tr>
        <td>Poblacion</td>
        <td><?= $datos["poblacion"]?></td>
    </tr>
</table>

