<?php

if($_POST){
    $boton = $_POST["boton"] ?: "ninguno";
    $texto = $_POST["texto"] ?: "";
    $resultado = "";
}

$textoSalida=$texto;

function caracteres(string $texto, string $caracter): int
{
    $resultado = 0;

    // for ($i = 0; $i < strlen($texto); $i++) {
    //     if ($texto[$i] == $caracter) {
    //         $resultado++;
    //     }
    // }
    $resultado = substr_count($texto, $caracter);
    return $resultado;
}

switch ($boton) {
    case 'a':
    case 'b':
    case 'c':
        $resultado = caracteres($texto, $boton);
        break;
    default:
        $resultado = "el caracter no es valido";
        break;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="post">
        <div>
            <label for="texto"></label>
            <input type="text" name="texto" id="texto" placeholder="escribe una frase">
        </div>
        <div>
            <button name="boton" value="a">a</button>
            <button name="boton" value="b">b</button>
            <button name="boton" value="c">c</button>
        </div>
    </form>
    <?= $resultado?>
</body>
</html>