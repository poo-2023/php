<?php

// -- UTILIZAMOS FETCH_ARRAY --

// mostrar todos los datos de la tabla personas
// de la base de datos personas1
// del servidor de base de datos que esta en localhost

// Quiero que utilicemos el metodo fetch_array
// utilizando un array asociativo para cada registro

require_once "funciones.php";
require_once "controlErrores.php";
// Definir la información de conexión a la base de datos
$host = "localhost";
$usuario = "root";
$contrasena = "";
$base_de_datos = "personas1";

// 1 paso 
// realizar la conexion
$conexion = new mysqli($host, $usuario, $contrasena, $base_de_datos);

// 2 paso 
// comprobar que la conexion se ha realizado correctamente

if ($conexion->connect_error) {
    die("La conexion ha fallado: " . $conexion->connect_error);
}

// 3 paso
// realizar la consulta
$consulta = "select * from personas";
$resultados = $conexion->query($consulta);

// 4 paso 
// colocar los resultados en pantalla
// utlizar el método fetch_array para leer los registros
// utilizamos un while

while($registro = $resultados->fetch_array(MYSQLI_ASSOC)) {
    // registro es un array unidimensional de toipo asociativo con los datos de un registro 

    echo detailView($registro);
}

// 5 paso
// cerrar la conexion

