<?php

class Humano{

    public $nombre;
    public $sexo;
    public $fechaNacimiento;
    public function presentarse($nombre,  $fechaNacimiento){
        return "Hola, soy " . $this->nombre . " y nací en " . $this->fechaNacimiento;
    }

    public function __construct($nombre="", $sexo="", $fechaNacimiento=""){
        $this->nombre=$nombre;
        $this->sexo=$sexo;
        $this->fechaNacimiento=$fechaNacimiento;
    }

}