<?php

// cargando la clase en el espacio de nombres actual
use clases\Aplicacion;
use clases\GridView;
use clases\Header;
use clases\Modelo;
use clases\Pagina;


// definiendo el autoload
spl_autoload_register(function ($clase) {
    include $clase . '.php';
});


// instanciando la clase
$aplicacion = new Aplicacion();
$favoritos = new Modelo($aplicacion->db);
$query = $favoritos->query("select * from favoritos");
$cabecera = Header::ejecutar([
    "titulo" => "Pagina de inicio",
    "subtitulo" => $aplicacion->configuraciones['autor'],
    "salida" => "Aplicacion para mostrar paginas interesantes"
]);

Pagina::comenzar();
?>

<?= GridView::render($query, [
    "ver" => "ver.php"
], [
    "id", "titulo", "url"
]) ?>

<?php
Pagina::terminar([
    "titulo" => "inicio",
    "cabecera" => $cabecera,
    "pie" => "Creado por: " . $aplicacion->configuraciones['autor']
]);

