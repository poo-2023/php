<?php

function operacion($a, $b) 
{
    return $a+$b;
}

function operacion1($a, $b, $r){
    $r=$a+$b;
    return $r;
}

function operacion2($a, $b){
    global $resultado;
    $resultado=$a+$b;
}

function operacion3($a, $b, &$r)
{
    $r=$a+$b;
}

$numero1=3;
$numero2=6;
$resultado=0;

$resultado=operacion($numero1, $numero2);
echo $resultado;

operacion1($numero1, $numero2, $resultado);