<?php
// arranco sesiones
session_start();

// cargo las funciones
require_once "funciones.php";

// control de errores
controlErrores();

// inicializo las variables
$salida = "";
$aviso = "";
$parametros = require_once "parametros.php";

// compruebo si usuario esta logueado
if (!isset($_SESSION["nombre"])) {
    // si no lo esta, redirijo a login
    header("Location: index.php");
} else {
    // si esta logueado
    $menu = menu([
        "Inicio" => "index.php",
        "Mensaje" => "mensaje.php",
        "Salir" => "salir.php",
    ]);

    // me conecto a la base de datos
    $conexion = new mysqli(
        $parametros["bd"]["servidor"],
        $parametros["bd"]["usuario"],
        $parametros["bd"]["password"],
        $parametros["bd"]["nombreBd"]
    );

    // compruebo si la conexion es correcta
    if ($conexion->connect_error) {
        die("Error de conexión: " . $conexion->connect_error);
    }

    // comprobar si he pulsado el boton de añadir mensaje (Enviar)
    if ($_POST) {
        // leer los datos del formulario
        $datos["mensaje"] = $_POST["mensaje"];

        // preparo la consulta
        $sql = "INSERT INTO mensaje (nombre,mensaje) 
        VALUES ('$_SESSION[nombre]','{$datos["mensaje"]}')";
        // ejecuto la consulta
        if ($conexion->query($sql)) {
            $aviso = "Mensaje añadido";
        } else {
            $aviso = "Error al insertar";
        }
    }

    // preparar el texto de la consulta
    $sql = "SELECT * FROM mensaje";

    // obtengo el mysqli_result
    $resultados = $conexion->query($sql);

    $salida = gridViewBotones($resultados, [
        "borrar" => "borrar.php"
    ]);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Mensajes</h1>
    <div>
        <?= $menu ?>
    </div>

    <div>
        <?php
        require "_form.php";
        ?>
    </div>

    <div>
        <?= $salida ?>
    </div>

    <div>
        <?= $aviso ?>
    </div>


</body>

</html>
