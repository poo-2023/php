<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="post">
        <div>
            <label for="email" name="email">Email</label>
            <input type="email" name="email">
        </div>
        <div>
            <label for="password" name="password">Password</label>
            <input type="password" name="password">
        </div>
        <div>
            <label for="mes">Mes de acceso</label>
            <input type="radio" value="mes" name="mes">Enero
            <input type="radio" value="mes" name="mes">Febrero
            <input type="radio" value="mes" name="mes">Marzo
        </div>
        <div>
            <label for="formato">Formato de acceso</label>
            <input type="checkbox" value="formato[]">Teléfono
            <input type="checkbox" value="formato[]">Ordenador
        </div>
        <div>
            <label for="ciudad">Ciudad</label>
            <input type="text">
        </div>
        <div>
            <label for="navegadores">Navegadires utilizados</label>
            <select name="navegador[]" id="navegador">
                <option value="google">Google</option>
                <option value="edge">Edge</option>
                <option value="safari">Safari</option>
            </select>
        </div>
        <div>
            <button type="submit">Enviar</button>
        </div>


    </form>
</body>
</html>