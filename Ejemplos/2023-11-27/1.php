<?php 
// crear variables de tipo numericas

$a=1;
$b=2.3;

// crear variabe de tipo string

$c="ejemplo";
$d="1";

// crear variable booleana

$e=true;
$f=false;

// crear variable de tipo array

$g=[1,2,"hola"];
$h=array(23,3);

// crear variable de tipo null

$i=null;

// para comprobar valor y tipo var_dump

var_dump($a,$d,$e,$h);

// para comprobar el tipo gettype

echo gettype($d);
// para comprobar el valor echo
// para imprimir impresión corta

echo "<div>d={$d}</div>";

?>     

<div>
    d=<?= $d ?>
</div>