<?php
// si no ha cargado el formulario le obligo a cargarlo
if (!$_POST) {
    header("Location: 003-formularioTresBotones.php");
}
//var_dump($_POST);

// $a = "ejemplo de clase";
// un string podeis leerle como un array
// echo $a[2];=>e
// podeis escribirle como un array
// $a[2]="x"=> "ejxmplo de clase";
// pero no podeis utilizar funciones de arrays
// count($a); ==> error
// foreach($a as $v){} ==> error

// conocer la longitud de un string
// strlen($a)

// funcion que recibe un texto y un caracter y me tiene que indicar
// el numero de veces que el caracter sale en el texto

var_dump($_POST);
function caracteres(string &$texto, string $caracter): int
{
    $resultado = 0;

    for ($i = 0; $i < strlen($texto); $i++) {
        if ($texto[$i] == $caracter) {
            $resultado++;
            $texto[$i] = "-";
        }
    }
    echo $texto;
    return $resultado;
}

$boton = $_POST["boton"] ?: "ninguno";
$texto = $_POST["texto"] ?: "";
$resultado = "";

// funciona pero es un poco arriesgado
// $resultado = caracteres($texto, $boton);

// if($boton=="a" || $boton=="b" || $boton=="c"){
//     $resultado = caracteres($texto, $boton);
// }else{
//     $resultado = "el caracter no es valido";
// }

switch ($boton) {
    case 'a':
    case 'b':
    case 'c':
        $resultado = caracteres($texto, $boton);
        break;
    default:
        $resultado = "el caracter no es valido";
        break;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div>
        <?= $texto ?>
    </div>
    <div>El caracter buscado es <?= $boton ?></div>
    <div>
        <?= $resultado ?>
    </div>
</body>

</html>
