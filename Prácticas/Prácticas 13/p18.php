<?php

$cadena="cadena";
function imprimirVocales(string $cadena): array
{
    $vocales=[];
    $partida=str_split($cadena);
    foreach ($partida as $caracter) {
        switch ($caracter) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                array_push($vocales);
            default:
                break;
        }
    }

    return $vocales;
};
