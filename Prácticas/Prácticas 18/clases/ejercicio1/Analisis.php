<?php

namespace clases\ejercicio1;

class Analisis{

    private string $valor;
    private int $longitud;
    private int $vocales;

    public function __construct(string $valor)
    {
        $this->setValor($valor);
    }

    // getters y setters

    public function getValor(): string
    {
        return $this->valor;
    }

    public function setValor(string $valor): void
    {
        $this->valor = $valor;
    }

    public function getLongitud(): int
    {
        return $this->longitud;
    }

    public function setLongitud(int $longitud): void
    {
        $this->longitud = $longitud;
    }

    public function contarVocal(string $valor)
    {
        foreach (str_split($valor) as $letra) {
            if ($letra == 'a' || $letra == 'e' || $letra == 'i' || $letra == 'o' || $letra == 'u') {
                $this->vocales++;
            }
        }
        return $this->vocales;
    }


}