<?php

namespace src;

class Empleado extends Persona{

    private ?float $sueldo;

    // sobreescribo el método mostrar de la clase Persona

    public function mostrar():string
    {
        $salida="<ul>";
        $salida.="<li>Nombre: " . $this->getNombre() . "</li>";
        $salida.="<li>Edad: " . $this->getEdad() . "</li>";
        $salida.="<li>Sueldo: " . $this->sueldo . "</li>";
        $salida.="</ul>";
        return $salida;
    }

    public function __construct()
    {
        parent :: __construct();
        $this->sueldo=null;
    }

    public  function calcularSuledo(int $horas): void
    {
        $this->sueldo=$horas * 10;
    }

}