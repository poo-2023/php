<?php

$parametros = require_once "parametros.php";
require_once "funciones.php";
controlErrores();

$conexion = @new mysqli(
    $parametros["servidor"],
    $parametros["usuario"],
    $parametros["password"],
    $parametros["nombreBd"]
);

// compruebo si la conexión es correcta
if($conexion->connect_error){
    die("Error de conexión: " . $conexion->connect_error);
}

// Crear la tabla personas si no existe
$sql = "CREATE TABLE IF NOT EXISTS empleados (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  nombre VARCHAR(30) NOT NULL,
  apellidos VARCHAR(30) NOT NULL,
  edad INT(3) NOT NULL,
  poblacion VARCHAR(30) NOT NULL,
  codigoPostal VARCHAR(5) NOT NULL,
  fechaNacimiento DATE NOT NULL
)";

if($conexion->query($sql)){
    echo "La tabla se ha creado correctamente";
}else{
    echo "Error creando la tabla: " . $conexion->error;
}

$conexion->close();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $parametros ["nombreAplicacion"] ?> </title>
</head>
<body>
    
</body>
</html>