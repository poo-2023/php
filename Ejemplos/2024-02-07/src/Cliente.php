<?php

namespace src;

class Cliente extends Persona{

    protected ?string $nombreEmpresa;
    protected ?string $telefono;
    
    // sobreescribir mostrar
    public function mostrar(): string
    {
        $salida="<ul>";
        $salida.="<li>Nombre: " . $this->getNombre() . "</li>";
        $salida.="<li>Edad: " . $this->getEdad() . "</li>";
        $salida.="<li>Nombre de la empresa: " . $this->nombreEmpresa . "</li>";
        $salida.="<li>Telefono: " . $this->telefono . "</li>";
        $salida.="</ul>";

        return $salida;

    }

    // getters y setters
    
    public function getNombreEmpresa(): string
    {
        return $this->nombreEmpresa;
    }

    public function setNombreEmpresa(string $nombreEmpresa): void
    {
        $this->nombreEmpresa = $nombreEmpresa;
    }

    public function getTelefono(): string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): void
    {
        $this->telefono = $telefono;
    }

    public function __construct()
    {
        parent :: __construct();
        $this->nombreEmpresa=null;
        $this->telefono=null;
        $this->propiedadesAsignacionMasiva=["nombre","edad", "nombreEmpresa", "telefono"];
    }

}