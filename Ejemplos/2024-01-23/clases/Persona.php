<?php
class Persona
{
    // definir los miembros de mi clase

    // atributos(propiedades) de la clase
    public $nombre = "";
    public $apellidos = "";
    public $edad = 0;

    // metodos de la clase
    public function hablar()
    {
        // mediante la palabra reserva $this podemos acceder a los miembros de la clase
        // dentro de la clase
        return "<br>Soy " . $this->nombre . " y bla bla bla";
    }
}

