<?php

namespace clases\fran;
class Numeros{
    public array $numeros;

    public function __construct(array $numeros=[])
    {
        $this->numeros=$numeros;
    }

    public function calcularMedia(): float{
        // usar foreach y array_sum

        $suma=array_sum($this->numeros);
        $media=$suma/count($this->numeros);

        return $media;
    }

    public function calcularMedia2(): float{

        foreach($this->numeros as $numero){
            $suma=0;
            $suma+=$numero;
        }

        return $suma/count($this->numeros);
    }

    public function calcularModa(): float{
        $moda=0;
        // array count values
        // función max array
        // array search
        return $moda;
    }

    public function calcularMediana(): float{
        $mediana=0;

        return $mediana;
    }
}