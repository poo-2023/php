<?php

// UTILIZAMOS FETCH_OBJECT
// mostrar todos los datos de la tabla personas
// de la base de datos personas1
// del servidor de base de datos que está en localhost

// usar método fetch_all
// usando un array asociativo para cada registro

use clases\Personaconstructor;

require_once "funciones.php";
require_once "autoload.php";
require_once "controlErrores.php";
//definir información de conexión a la base de datos

$host = "localhost";
$usuario = "root";
$contrasena = "";
$base_de_datos = "personas1";

// 1 paso 
// realizar la conexion
$conexion = new mysqli($host, $usuario, $contrasena, $base_de_datos);

// 2 paso 
// comprobar que la conexion se ha realizado correctamente

if ($conexion->connect_error) {
    die("La conexion ha fallado: " . $conexion->connect_error);
}

// 3 paso
// realizar la consulta
$consulta = "select * from personas";
$resultados = $conexion->query($consulta);

// 4 paso 
// colocar los resultados en pantalla
// utlizar el método fetch_array para leer los registros
// utilizamos un while
// cuando utilizamos el método fetch_object 
// si le pasamos una clase, esta no debe tener el constructor


while($registro = $resultados->fetch_object("clases\Persona")){
    $registros[] = new Personaconstructor(); // array de objetos

    $registros[] = $registro;
}


echo gridViewTable($registros);
// en registro tenemos un objeto stdClass 
// ahí tenemos los datos de un registro


// 5 paso
// cerrar la conexion

$conexion -> close();