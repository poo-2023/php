<?php

//contador de visitas que cuando llegue a 10 se reinicie
session_start();

if(isset($_SESSION["visitas"])){
    $_SESSION["visitas"]++;
}else{
    $_SESSION["visitas"]=1;
}

echo $_SESSION["visitas"];

if( $_SESSION["visitas"]>=10){
    session_unset(); // cierra la sesión y borra todas las variables de sesión
}

echo "<br>";
echo $_SESSION["visitas"];