<?php

use src\Empleado;
use src\Cliente;
use src\Directivo;

require 'autoload.php';

$empleados=[];

$empleados[0]=new Empleado();
$empleados[0]->setNombre("Ana");
$empleados[0]->setEdad(30);
$empleados[0]->calcularSuledo(54);
echo $empleados[0]->mostrar();

$empleados[1]=new Empleado();
$empleados[1]->setNombre("Luisa");
$empleados[1]->setEdad(45);
$empleados[1]->calcularSuledo(45);
echo $empleados[1]->mostrar();

$clientes=[];

$clientes[0]=new Cliente();
$clientes[0]->setNombre("Cesar");
$clientes[0]->setEdad(35);
$clientes[0]->setNombreEmpresa("Alpe");
$clientes[0]->setTelefono("t1");
echo $clientes[0];

$directivos=[];
$directivos[0]=new Directivo();
$directivos[0]->setNombre("Jose");
$directivos[0]->setEdad(30);
$directivos[0]->calcularSuledo(5);
$directivos[0]->setCategoria("A");
echo $directivos[0];