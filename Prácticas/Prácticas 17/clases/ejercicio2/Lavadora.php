<?php

namespace clases\ejercicio2;

use clases\ejercicio1\Electrodomestico;

class Lavadora extends Electrodomestico{
    public float $precio;
    public bool $aguaCaliente;


    public function __construct(string $tipo, string $marca, float $potencia, float $precio, bool $aguaCaliente)
    {
        parent::__construct($marca, $tipo, $potencia);
        $this->aguaCaliente = $aguaCaliente;
        $this->precio = $precio;
    }

    // getter y setters

    public function getPotencia(): float
    {
        return $this->potencia;
    }

    public function setPotencia(float $potencia): void
    {
        $this->potencia = $potencia;
    }

    public function getTipo(): string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): void
    {
        $this->tipo = $tipo;
    }

    public function getMarca(): string
    {
        return $this->marca;
    }

    public function setMarca(string $marca): void
    {
        $this->marca = $marca;
    }

    public function getPrecio(): float
    {
        return $this->precio;
    }

    public function setPrecio(float $precio): void
    {
        $this->precio = $precio;
    }

    public function getAguaCaliente(): bool
    {
        return $this->aguaCaliente;
    }

    public function setAguaCaliente(bool $aguaCaliente): void
    {
        $this->aguaCaliente = $aguaCaliente;
    }

    public function toString()
    {
        return "Tipo: " . $this->tipo . ", Marca: " . $this->marca . ", Potencia: " . $this->potencia . ", Precio: " . $this->precio . ", Agua caliente: " . $this->aguaCaliente;
    }
    public function getConsumo(int $horas)
    {
        $precioCaliente=$this->potencia+$this->potencia*0.2;

        if($aguaCaliente=true){
            return "El consumo es " . $horas * $precioCaliente;
        }else{
            return "El consumo es " . $horas * $this->potencia;
        }
    }


    public function getCosteConsumo(int $horas, float $coste){
        return "El coste es " . $this->potencia * $horas * $coste;
    }


}