<?php

$datos=[
    "nombre"=>"Pepe",
    "apellido"=>"Perez",
    "edad"=>null,
];

// usando el operador if/else 
// realizar las siguientes asignaciones

// $nombre="Pepe";
// $apellidos="no conocido";
// $edad="no conocida";

if(isset($datos["nombre"])){
    $datos["nombre"]="Pepe"; 
}else{
    echo "no hay datos";
}

if(isset($datos["apellido"])){
    $datos["apellido"]="no conocido";
}else{
    echo "no hay datos";
}

if(isset($datos["edad"])){
    $datos["edad"]="no conocida";
}else{
    echo "no hay datos";
}

var_dump($datos);

// lo mismo con ternario normal ?

echo isset($datos["nombre"]) ? $datos["nombre"]="Pepe" : "no hay datos";
echo isset($datos["apellido"]) ? $datos["apellido"]="no conocido" : "no hay datos";
echo isset($datos["edad"]) ? $datos["edad"]="no conocida" : "no hay datos";

// otra vez pero con ternario recortado ??

echo $datos["nombre"]="Pepe" ?? "no hay datos";
echo $datos["apellido"]="no conocido" ?? "no hay datos";
echo $datos["edad"]="no conocida" ?? "no hay datos";