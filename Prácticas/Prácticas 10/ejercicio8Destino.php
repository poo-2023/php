<?php

$radio=$_GET["radio"];
$longitud=pi()*($radio*2);

echo "La longitud es {$longitud}<br>";

$area=pi()*pow($radio, 2);

echo "El área es {$area}<br>";

$volumen=(4/3)*pi()*pow($radio, 3);

echo "El volumen de la esfera es {$volumen}<br>";
