<?php
// datos del alumno con la nota más alta

// no es necesario escribir el índice del array
$datos=[
    0=> 12,
    1=> "Eva",
    2=>"Gomez Palomo",
    3=>"Laredo"
];

?>

<table>
    <tr>
        <td>Campo</td>
        <td>Valor</td>
    </tr>
    <tr>
        <td>ID</td>
        <td><?= $datos[0]?></td>
    </tr>
    <tr>
        <td>Nombre</td>
        <td><?= $datos[1]?></td>
    </tr>
    <tr>
        <td>Apellidos</td>
        <td><?= $datos[2]?></td>
    </tr>
    <tr>
        <td>Poblacion</td>
        <td><?= $datos[3]?></td>
    </tr>
</table>

