<?php

$numero=1;
$texto="1,2,3,4,1,2,3";

// contar número de veces que sale $número en $texto

// for

for ($i=0; $i < strlen($texto); $i++) { 
    if($texto[$i]== $numero){
        $repeticiones++;
    }
}

echo $repeticiones;

// foreach
// debemos convertir el string en array

$textoArray=explode(",", $texto);
$repeticiones=0;

foreach ($textoArray as $valor) {
    if($valor==$numero){
        $repeticiones++;
    }
}

// array count values

array_count_values($textoArray);

//subsrt_count

$repeticiones=0;
$repeticiones=substr_count($texto, $numero);
echo $repeticiones;