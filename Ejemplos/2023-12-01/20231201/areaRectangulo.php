<?php
// funcion que recibe como argumento el radio de un circulo y 
// devuelve el area de ese circulo
function areaRectangulo(int $lado): float
{
    return pow($lado, 2);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Calcular el área de un rectángulo</h1>
    <?php
    // genero un circulo de area aleatoria entre 10 y 150
    $lado = $GET_["lado"];

    // dibujo el circulo de ese radio
    // aprovecho el ejemplo anterior
    // mando el radio por get en la url

    echo "<img src='007DibujarGD.php'>";

    // calculo el area del circulo
    $area = areaRectangulo($lado);

    // mostrar el radio y el area calculada
    ?>
    <ul>
        <li>Lado: <?= $lado?></li>
        <li>Area: <?= areaRectangulo($lado)?></li>
    </ul>


</body>

</html>