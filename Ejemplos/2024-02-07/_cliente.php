<h2>Clientes</h2>

<form method="post">
    <div>
        <label for="nombre">
            Nombre
        </label>
        <input type="text" name="nombre" id="nombre">
    </div>
    <br>
    <div>
        <label for="edad">
            Edad
        </label>
        <input type="number" name="edad" id="edad">
    </div>
    <br>
    <div>
        <label for="nombreEmpresa">
            Nombre de empresa
        </label>
        <input type="text" name="nombreEmpresa" id="nombreEmpresa">
    </div>
    <br>
    <div>
        <label for="telefono">
            Teléfono de contacto
        </label>
        <input type="text" name="telefono" id="telefono">
    </div>
    <br>
    <div>
        <button type="submit" name="cliente">
            Almacenar
        </button>
        <button type="reset">
            Limpiar
        </button>

    </div>
</form>
