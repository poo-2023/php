<?php

// usar espacios de nombres
// para poder tener la misma clase en varias carpetas
// las clases organizadas en carpetas separadas

spl_autoload_register(function($clase){
    include $clase . ".php";
});

$objeto1=new clases\profesor\Numeros([2,3,5]);

$objeto2=new clases\fran\Numeros([7,4,2]);

var_dump($objeto2);

echo $objeto2->calcularMedia();