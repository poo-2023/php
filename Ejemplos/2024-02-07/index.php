<?php
// carga la clase empleado en el espacio de nombres actual
use src\Empleado;
use src\Cliente;
use src\Directivo;

// para poder usar sesiones
session_start();

// para carga automática de clases
require_once "autoload.php";

// he pulsado el boton de almacenar empleado
if (isset($_POST["empleado"])) {
    // tengo que pasar los datos del array POST (asociativo) a un objeto de tipo empleado
    $empleado = new Empleado();
    $empleado->asignar($_POST);
}

if(isset($_POST["cliente"])) {
    $cliente = new Cliente();
    $cliente->asignar($_POST);
}

if(isset($_POST["directivo"])) {
    $cliente = new Directivo();
    $cliente->asignar($_POST);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    // cargamos formulario
    require "_empleado.php";

    echo "<hr>";

    require "_cliente.php";

    echo "<hr>";

    require "_directivo.php";
?>
<h2>Últimos registros introducidos</h2>
<?php
    // si está instanciado, imprime el toString
    if(isset($empleado)){
        echo $empleado;
        // guardo el objeto en la sesión serializado
        $_SESSION["empleado"][]=serialize($empleado);
    }

    if(isset($cliente)){
        echo $cliente;
        // guardo el objeto en la sesión serializado
        $_SESSION["cliente"][]=serialize($cliente);
    }

    if(isset($directivo)){
        echo $directivo;
        // guardo el objeto en la sesión serializado
        $_SESSION["directivo"][]=serialize($directivo);
    }
    // quiero que muestre todos los registros de la sesión actual

    if(isset($_SESSION["empleado"])){
        foreach ($_SESSION["empleado"] as $empleado) {
            $empleado = unserialize($empleado); // deserializo el objeto
            echo $empleado; // llamo al toString
        }
    }else{
        echo "No hay empleados";
    }

    echo "<br>";

    if(isset($_SESSION["cliente"])){
        foreach ($_SESSION["cliente"] as $cliente) {
            $cliente = unserialize($cliente); // deserializo el objeto
            echo $cliente; // llamo al toString
        }
    }else{
        echo "No hay clientes";
    }

    echo "<br>";

    if(isset($_SESSION["directivo"])){
        foreach ($_SESSION["directivo"] as $directivo) {
            $directivo = unserialize($directivo); // deserializo el objeto
            echo $directivo; // llamo al toString
        }
    }else{
        echo "No hay directivos";
    }
    ?>
</body>

</html>
