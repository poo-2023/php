<?php

// voy a realizar un gridViewTable mas versatil

// este gridview utiliza fetch_all
/**
 *
 * Funcion que retorna una tabla con los registros pasados y solo los campos
 * que queramos. Los registros se los tengo que pasar como un objeto de tipo
 * mysqli_result
 * @param mysqli_result $resultados un objeto de tipo mysqli_result
 * @param array $camposMostrar
 * @return string tabla a mostrar
 */
// function gridViewTable(mysqli_result $resultados, array $camposMostrar = null): string
// {
//     // creo un array con los registros
//     $registros = $resultados->fetch_all(MYSQLI_ASSOC);
//     // comprobar si hay registros
//     if (count($registros) > 0) {
//         if (is_null($camposMostrar)) {
//             // leo los campos a mostrar si no los paso como argumento
//             // los campos los leo del primer registro
//             $camposMostrar = array_keys($registros[0]);
//         }
//         $salida = "<table border='1'>";
//         // añadir los nombres de los campos a mostrar

//         // cabecera
//         $salida .= "<thead>
//         <tr>";
//         foreach ($camposMostrar as $campo) {
//             $salida .= "<td>" . $campo . "</td>";
//         }
//         $salida .= "</tr>
//     </thead>";

//         // datos, registros
//         foreach ($registros as $registro) {
//             // cada fila
//             $salida .= "<tr>";
//             // campos
//             foreach ($camposMostrar as $campo) {
//                 $salida .= "<td>" . $registro[$campo] . "</td>";
//             }
//             $salida .= "</tr>";
//         }

//         $salida .= "</table>";
//     } else {
//         $salida = "<div>No hay registros a mostrar</div>";
//     }
//     return $salida;
// }


// en este gridView voy a utilizar fetch_array
// creando un array con todos los registros
// function gridViewTable(mysqli_result $resultados, array $camposMostrar = null): string
// {
//     // creo un array con los registros
//     while ($registro = $resultados->fetch_array(MYSQLI_ASSOC)) {
//         $registros[] = $registro;
//     }

//     // comprobar si hay registros
//     if (count($registros) > 0) {
//         if (is_null($camposMostrar)) {
//             // leo los campos a mostrar si no los paso como argumento
//             // los campos los leo del primer registro
//             $camposMostrar = array_keys($registros[0]);
//         }
//         $salida = "<table border='1'>";
//         // añadir los nombres de los campos a mostrar

//         // cabecera
//         $salida .= "<thead>
//         <tr>";
//         foreach ($camposMostrar as $campo) {
//             $salida .= "<td>" . $campo . "</td>";
//         }
//         $salida .= "</tr>
//     </thead>";

//         // datos, registros
//         foreach ($registros as $registro) {
//             // cada fila
//             $salida .= "<tr>";
//             // campos
//             foreach ($camposMostrar as $campo) {
//                 $salida .= "<td>" . $registro[$campo] . "</td>";
//             }
//             $salida .= "</tr>";
//         }

//         $salida .= "</table>";
//     } else {
//         $salida = "<div>No hay registros a mostrar</div>";
//     }
//     return $salida;
// }


// en este gridView voy a utilizar fetch_array
// sin crear un array con todo los registros

function gridViewTable(mysqli_result $resultados, array $camposMostrar = null): string
{
    // comprobar si hay registros
    if ($resultados->num_rows > 0) {
        // leo el primer registro en un array unidimensional asociativo
        $registro = $resultados->fetch_array(MYSQLI_ASSOC);
        if (is_null($camposMostrar)) {
            // leo todos los campos
            $camposMostrar = array_keys($registro);
        }
        $salida = "<table border='1'>";

        // cabecera
        $salida .= "<thead><tr>";
        foreach ($camposMostrar as $campo) {
            $salida .= "<td>" . $campo . "</td>";
        }
        $salida .= "</tr></thead>";

        // muestro el primer registro
        $salida .= "<tr>";
        foreach ($camposMostrar as $campo) {
            $salida .= "<td>" . $registro[$campo] . "</td>";
        }
        $salida .= "</tr>";
        // creo un array con los registros
        while ($registro = $resultados->fetch_array()) {
            $salida .= "<tr>";
            foreach ($camposMostrar as $campo) {
                $salida .= "<td>" . $registro[$campo] . "</td>";
            }
            $salida .= "</tr>";
        }
        $salida .= "</table>";
    } else {
        $salida = "<div>No hay registros a mostrar</div>";
    }
    return $salida;
}

