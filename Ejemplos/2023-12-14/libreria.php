<?php

// crear una función de php que me permita conocer el número
// de veces que el primer argumento se repite en el segundo
// que es un array

function repite($valor, array $vector): int
{
    $veces=0;
    foreach ($vector as $value) {
        if($value==$valor){
            $veces++;
        }
    }
    return $veces;
}

// crear una función para conocer el número de vocales que hay en un array de caracteres
// vocales en minúscula y sin tildes
function contarVocales(array $vector): int
{
    $veces=0;
    foreach ($vector as $value) {
        switch (strtolower($value)) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                $veces++;
                break;
        }
    }
    return $veces;
}