<?php

// UTILIZAMOS FETCH_A
// mostrar todos los datos de la tabla personas
// de la base de datos personas1
// del servidor de base de datos que está en localhost

// usar método fetch_all
// usando un array asociativo para cada registro

// definir información de conexión a la base de datos

// cargar libreria con funciones para mostrar información
require_once "funcionesObjetos.php";
require_once "controlErrores.php";

$host = "localhost";
$usuario = "root";
$contrasena = "";
$base_de_datos = "personas1";

// paso 1
// realizar la conexión

$conexion = new mysqli($host, $usuario, $contrasena, $base_de_datos);

// paso 2
// comprobar que la conexión se ha realizado correctamente


// paso 3 
// realizar la consulta

$consulta = "select * from personas";
$resultados = $conexion ->query($consulta);

// lamo a gridviewtable y le paso la consulta 
// sin ejecutar como mysqli_result

echo gridViewTable($resultados);