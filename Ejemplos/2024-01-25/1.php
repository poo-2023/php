<?php

//precarga de clases

spl_autoload_register(function($clase){
    include "clases/" . $clase . ".php";
});

//creo objetos de tipo coche

$ford1=new Coche();

$ford2=new Coche("rojo", 5, "ford");

var_dump($ford1);

var_dump($ford2);