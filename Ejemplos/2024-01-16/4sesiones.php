<?php

//contador de visitas que cuando llegue a 10 se reinicie
session_start();

if(isset($_SESSION["visitas"])){
    $_SESSION["visitas"]++;
}else{
    $_SESSION["visitas"]=1;
}

echo $_SESSION["visitas"];

if( $_SESSION["visitas"]>=10){
    session_abort(); // la sesión no se reinicia, pero se dejan de guardar los datos de la sesión
}

echo "<br>";
echo $_SESSION["visitas"];