<?php

// esta función toma un argumento el nombre de un archivo, se lo da a una variable
// crea un array con los nombres de los directorios y ficheros


function leerDirectorios($handle = "."){
    $handle=opendir($handle);
    while (false !== ($archivo=readdir($handle))){
        $archivos[]=strtolower($archivo);
    }
    closedir($handle);
    sort($archivos);
    return $archivos;
}