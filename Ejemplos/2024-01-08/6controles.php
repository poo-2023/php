<?php

// controlar que he pulsado el botón de enviar
// (se puede hacer nombrando el botón y usando isset)

if($_POST){

    // leyendo los datos
    $nombre=$_POST["nombre"] ?: "";
    $apellidos=implode(" ", $_POST["apellidos"]);
    // controlo si no he seleccionado nada con un ternario nullable
    $estado=isset($_POST["estado"]) ?? "";
    // controlo si no he seleccionado nada con un ternario
    $aficiones=isset($_POST["aficiones"]) ? implode(" ", $_POST["aficiones"]) : "";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="post">
        <div>
            <label for="nombre">Nombre</label>
            <input type="text" id="nombre" name="nombre">
        </div>
        <div>
            <label for="apellido1">Apellido 1</label>
            <input type="text" id="apellido1" name="apellidos[]">
        </div>
        <div>
            <label for="apellido2">Apellido 2</label>
            <input type="text" id="apellido2" name="apellidos[]">
        </div>
        <div>
            <span>Estado civil</span>   <br>
            <input type="radio" value="soltero" name="estado">
            <label for="soltero">Soltero/a</label>  
            <input type="radio" value="casado" name="estado">
            <label for="casado">Casado/a</label>
        </div>
        <div>
            <span>Aficiones</span>  <br>
            <label for="deportes">Deportes</label>
            <input type="checkbox" id="deportes" value="deportes" name="aficiones[]">  
            <label for="lectura">Lectura</label>
            <input type="checkbox" id="lectura" value="lectura" name="aficiones[]">
            <label for="dormir">Dormir</label>  
            <input type="checkbox" id="dormir" value="dormir" name="aficiones[]"> 
        </div>
        <div>
            <button>Enviar</button>
            <button type="reset">Borrar</button>
        </div>
    </form>

    <?php
    if($_POST){
    ?>
        <div>
            <p>Nombre: <?= $nombre ?></p>
            <p>Apellidos: <?= $apellidos ?></p>
            <p>Estado civil: <?= $estado ?></p>
            <p>Aficiones: <?= $aficiones ?></p>
        </div>
    <?php
    }
    ?>

</body>
</html>