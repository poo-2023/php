<?php

    /**
     * función que calcula una operación
     * @param mixed $datos Array cuyo primer y segundo elemento son números,y el tercer elemento es un csv 
     * @return array $resultados
     * devolver número de elementos con número 1 y 2. Suma de los dos números
     */

    //  comprobamos que el botón se haya pulsado
    if(isset($_POST["boton"])){
        // asignamos variables a los datos de l formulario
        $numero1 = $_POST["numero1"] ?: 0;
        $numero2 = $_POST["numero2"] ?: 0;
        $operacion = $_POST["operacion"] ?: "";
    }
    


    function operacion(...$datos)
    {
        $resultado=[0,0,0];

        $resultado[0]=subsrt_count($datos[2], $datos[0]);

        $resultado[1]=substr_count($datos[2], $datos[1]);

        $resultado[2]=$datos[1]+$datos[0];

        return $resultado;
    }

    $resultado=[];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
<form method="post">

    <div>
        <label for="numero1" id="numero1" name="numero1">Número 1</label>
        <input type="number" required>
    </div>
    <div>
        <label for="numero2" id="numero2" name="numero2">Número 2</label>
        <input type="number" required>
    </div>
    <div>
        <label for="operacion" id="operacion" name="operacion">Operación</label>
        <input type="text" required>
    </div>
    <div>
        <button type="submit" name="boton">Enviar</button>
        <button type="reset">Borrar</button>
    </div>
    
</form>
</body>
</html>