<?php

// arranco sesiones
session_start();

// cargo las funciones
require_once "funciones.php";

// control de errores
controlErrores();

// inicializo las variables
$parametros = require_once "parametros.php";

// me conecto a la base de datos
$conexion = new mysqli(
    $parametros["bd"]["servidor"],
    $parametros["bd"]["usuario"],
    $parametros["bd"]["password"],
    $parametros["bd"]["nombreBd"]
);

// compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}

// compruebo si esta logueado
if (isset($_SESSION["nombre"])) {
    // compruebo si ha pulsado en el mensaje que quiere eliminar
    if (isset($_GET["id"])) {
        // preparo la consulta para eliminar el mensaje
        $sql = "DELETE FROM mensaje WHERE id = " . $_GET["id"];
        $conexion->query($sql);
    }
    // redirecciono a la pagina mensaje
    header("Location: mensaje.php");
} else {
    // redirijo a la pagina de login
    header("Location: index.php");
}

