<?php
// inicializo las variables en el controlador
$numero1=0;
$numero2=0;
$numeros="";
$suma=0;
$numerosMostrar="";

// si he pulsado el botón de enviar
if($_POST){
    // almacena los números introducidos
    $numero1=$_POST["numero1"] ?: 0;
    $numero2=$_POST["numero2"] ?: 0;
    $numeros=$_POST["numeros"] ?: "0";
    $numerosArray=explode(";", $_POST["numeros"]);

    //calculo suma
    $suma=$numero1+$numero2+array_sum($numerosArray);

    $numeros=implode("<br>", $numerosArray);
    $numerosMostrar="{numero1}<br>{numero2}<br>{numeros}";
}