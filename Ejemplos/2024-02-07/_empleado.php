<h2>Empleados</h2>

<form method="post">
    <div>
        <label for="nombre">
            Nombre
        </label>
        <input type="text" name="nombre" id="nombre">
    </div>
    <br>
    <div>
        <label for="edad">
            Edad
        </label>
        <input type="number" name="edad" id="edad">
    </div>
    <br>
    <div>
        <label for="horas">
            Horas
        </label>
        <input type="number" name="horas" id="horas">
    </div>
    <br>
    <div>
        <button type="submit" name="empleado">
            Almacenar
        </button>
        <button type="reset">
            Limpiar
        </button>

    </div>
</form>
