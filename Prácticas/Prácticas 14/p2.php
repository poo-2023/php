<?php

function mostrar(...$valores){
    foreach($valores as $valor){
        echo "<br />$valor<br />";
    }
}

$dia=23;
$mes=1;
$year=2004;
$fecha=mktime(0,0,0,$mes,$dia,$year);
mostrar(date("d/m/y",$fecha));

$dia=35;
$mes=1;
$year=2004;
if(checkdate($mes, $dia, $year)){
    $fecha=mktime(0,0,0,$mes, $dia, $year);
}else{
    $fecha=mktime(0,0,0,12,1,2000);
}


mostrar(date("d/m/y", $fecha));

