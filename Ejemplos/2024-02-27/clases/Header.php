<?php
namespace clases;

class Header extends Widget{

    public static function ejecutar(array $parametros = [], string $layout = 'componentes/jumbotron'): string
    {
        return parent::ejecutar($parametros, $layout);
    }

    public static function terminar(array $parametros = [], string $layout = 'componentes/jumbotron'): void
    {
        // termina el flujo de salida
        // para que no se muestre el HTML
        // en el navegador
        $contenido = ob_get_clean();
        extract ($parametros);
        require "layouts/" . $layout . ".php";
    }

}