<?php

$parametros = require_once "parametros.php";

require_once "funciones.php";

// desactivar errores
controlErrores();

if (!isset($_GET["id"])) {
    header("Location: 008-listarRegistrosUpdateDelete.php");
}

// conexion a base de datos
$conexion = @new mysqli(
    $parametros["servidor"],
    $parametros["usuario"],
    $parametros["password"],
    $parametros["nombreBd"]
);

// compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}

/*
    DELETE FROM empleados WHERE id=1;
*/

// el registro a borrar me llega por GET
$sql = "DELETE FROM empleados WHERE id=" . $_GET["id"];

if ($conexion->query($sql)) {
    $salida = "Registro eliminado correctamente";
    $salida .= "<br><a href='008-listarRegistrosUpdateDelete.php'>Volver al listado</a>";
} else {
    $salida = "Error al eliminar el registro: " . $conexion->error;
}

$conexion->close();


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $parametros["nombreAplicacion"] ?></title>
</head>

<body>
    <h1><?= $parametros["nombreAplicacion"] ?></h1>
    <div>
        <?= $salida ?>
    </div>

</body>

</html>
