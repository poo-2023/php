<?php
// cargo la libreria (fichero de php con funciones)
// para cargar las librerias es mejor require_once que require (solo carga la página una vez)
require_once "libreria.php";

// no se debe usar include en este caso
// al no cargar la libreria no tengo las funciones necesarias
include_once "libreria.php";

// quiero saber el número de veces que se repite
// el valor 2 en el siguiente vector
$datos=[2,3,4,2,3,2,2];

echo repite(2, $datos);