<?php

$a=10;
$b=3;


//  el if ejecuta el primer echo porque $a es par

if(($a%2)==0){
    echo "punto 1";
}else{
    echo "punto 2";
}

// el echo "punto 3" se ejecuta en cualquier caso
echo "punto 3";

// el bucle ejecuta el else "punto 2" al $b ser impar
if(($b%2)==0){
    echo "punto 1";
}else{
    echo "punto 2";
}

echo "punto 3";
// el echo "punto 3" se ejecuta en cualquier caso


$vocales=["a", "e", "i", "o", "u"];

// var dump muestra en pantalla el contenido del array $vocales
var_dump($vocales);