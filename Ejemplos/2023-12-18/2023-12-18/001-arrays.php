<?php

// Crear una funcion llamada recorrer
// que reciba como primer parametro un array
// debe devolver una lista de html con todos los elementos del array
// para recorrer el array utilizar for
// observaciones
// solo debe funcionar con arrays una sola dimension
// solo con arrays enumerados secuenciales

/**
 * Genera una cadena HTML de una lista desordenada a partir de un array de datos.
 *
 * @param array $datos El array de datos para generar la lista. (array enumerado secuencial)
 * @return string La cadena HTML que representa la lista desordenada.
 */
function recorrer(array $datos): string
{
    $lista = "<ul>";
    for ($i = 0; $i < count($datos); $i++) {
        $lista .= "<li>{$datos[$i]}</li>";
    }
    $lista .= "</ul>";

    return $lista;
}


// Crear una funcion llamada recorrerV1 
// que reciba como primer parametro un array 
// debe devolver una lista de html con todos los elementos del array
// para recorrer el array utilizar for
// observaciones
// solo debe funcionar con arrays una sola dimension
// funciona con arrays enumerados y asociativos
// pista : array_values

function recorrerV1(array $datos): string
{
    // añado esta linea 
    // para colocar el array enumerado de forma secuencial
    $datos = array_values($datos);
    // return recorrer($datos);
    $lista = "<ul>";
    for ($i = 0; $i < count($datos); $i++) {
        $lista .= "<li>{$datos[$i]}</li>";
    }
    $lista .= "</ul>";
    return $lista;
}

// Crear una funcion llamada recorrer2
// que reciba como primer parametro un array 
// debe devolver una lista de html con todos los elementos del array
// para recorrer el array utilizar foreach
// observaciones
// solo debe funcionar con arrays una sola dimension
// debe funcionar con arrays asociativos y enumerados

function recorrer2(array $datos): string
{
    $lista = "<ul>";
    foreach ($datos as $valor) {
        $lista .= "<li>{$valor}</li>";
    }
    $lista .= "</ul>";

    return $lista;
}

// crear una funcion llamada recorrer3
// que reciba como primer parametro un array 
// debe devolver una lista de html con todos los elementos del array
// para recorrer el array utilizar while
// observaciones
// solo debe funcionar con arrays una sola dimension
// funciona con array enumerados y asociativos

function recorrer3(array $datos): string
{
    // creamos un array enumerado secuencial
    $datos = array_values($datos);

    $lista = "<ul>";
    $i = 0;
    while ($i < count($datos)) {
        $lista .= "<li>{$datos[$i]}</li>";
        $i++;
    }
    $lista .= "</ul>";
    return $lista;
}

// crear una funcion llamada recorrer4
// que reciba como primer parametro un array 
// debe devolver una lista de html con todos los elementos del array
// para recorrer el array utilizar array_walk

function recorrer4(array $datos): string
{
    $lista = "<ul>";
    array_walk($datos, function ($valor, $indice) use (&$lista) {
        $lista .= "<li>{$valor}</li>";
    });
    $lista .= "</ul>";
    return $lista;
}

// crear una funcion llamada recorrer4V1
// que reciba como primer parametro un array 
// debe devolver una lista de html con todos los elementos del array
// para recorrer el array utilizar array_walk
// con array_walk añadir a cada elemento del array una etiqueta <li>valor</li>
// utilizando implode

function recorrer4V1(array $datos):string
{
    $lista="<ul>";

    array_walk($datos, function(&$valor, $indice){
        $valor="<li>{$valor}</li>";
    });

    $lista .=implode("", $datos);
    $lista .="<ul>";
    
    return $lista;
}

// crear una funcion llamada recorrer5
// que reciba como primer parametro un array 
// debe devolver una lista de html con todos los elementos del array
// para recorrer el array utilizar array_map 
// con array_map añadir a cada elemento del array una etiqueta <li>valor</li>
// para unir el array nuevo utilizar implode

function recorrer5(array $datos): string
{
    $lista = "<ul>";

    // creo un array con los valores del array original colocandole un li a cada uno
    $datos1 = array_map(function ($valor) {
        return "<li>{$valor}</li>";
    }, $datos);

    // junto el nuevo array en un string y se lo añado a la lista
    $lista .= implode("", $datos1);
    // cierro la lista
    $lista .= "</ul>";

    return $lista;
}
