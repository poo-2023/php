<?php
class Persona
{
    // definir los miembros de mi clase

    // atributos de la clase
    public $nombre = "";
    public $apellidos = "";
    public $edad = 0;

    // metodos de la clase
    public function hablar()
    {
        // mediante la palabra reserva $this podemos acceder a los miembros de la clase
        // dentro de la clase
        return "<br>Soy " . $this->nombre . " y bla bla bla";
    }
}

// instancia de la clase
// creando un objeto de tipo Persona
$persona1 = new Persona();
$persona2 = new Persona();

// asignando valores a los atributos de la primera persona
$persona1->nombre = "Juan";
$persona1->apellidos = "Perez";
$persona1->edad = 30;

// asignado valores a los atributos de la segunda persona
$persona2->nombre = "Maria";
$persona2->apellidos = "Lopez";
$persona2->edad = 25;

// leer los atributos de la primera persona
echo "El nombre de la primera persona es " . $persona1->nombre;
echo "<br>Los apellidos de la primera persona son " . $persona1->apellidos;

// acceder al metodo de la primera persona
echo $persona1->hablar();

// acceder al metodo de la segunda persona
echo $persona2->hablar();
