DROP DATABASE IF EXISTS mensajes;
CREATE DATABASE mensajes;
USE mensajes;

CREATE TABLE mensaje(
  id int AUTO_INCREMENT,
  nombre varchar(200),
  mensaje varchar(500),
  fecha datetime, 
  PRIMARY KEY(id)
);
 
