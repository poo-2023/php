<?php

$parametros = require_once "parametros.php";

require_once "funciones.php";

// desactivar errores
controlErrores();

// conexion a base de datos
$conexion = @new mysqli(
    $parametros["servidor"],
    $parametros["usuario"],
    $parametros["password"],
    $parametros["nombreBd"]
);

// compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}

$sql = "select * from empleados";

if ($resultados = $conexion->query($sql)) {
    $salida = gridViewBotones($resultados);
} else {
    $salida = "Error al ejecutar la consulta: " . $conexion->error;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $parametros["nombreAplicacion"] ?></title>
</head>

<body>
    <h1><?= $parametros["nombreAplicacion"] ?></h1>
    <div>
        <?= $salida ?>
    </div>

</body>

</html>
