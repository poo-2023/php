<?php


$vocales = [
    "a","e","i","o","u"
    ];

// mostrar el numero de elementos de vocales utilizando una función
echo count($vocales);

// mostrar una vocal aleatoria utilizando mt_rand
echo $vocales[mt_rand(0,count($vocales)-1)];

// mostrar otra vocal aleatoria utilizando mt_rand
$numVocales=count($vocales)-1;
echo mt_rand(0,$numVocales);