<?php
require_once "funciones.php";

// cargo los parametros de aplicacion
$parametros = require_once("parametros.php");

// desactivar errores
controlErrores();

// preparo la salida de la vista
$salida = "";

// conexion a base de datos
$conexion = @new mysqli(
    $parametros["bd"]["servidor"],
    $parametros["bd"]["usuario"],
    $parametros["bd"]["password"],
    $parametros["bd"]["nombreBd"]
);

// compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}

// array con los datos de la consulta
$datos = [
    "id" => "",
    "nombre" => 0,
    "mensaje" => "",
    "fechaHora" => "",
];

// tengo que comprobar si vengo o no de pulsar el boton insertar en el formulario
if ($_POST) {
    // si he pulsado el boton leo los datos que envio desde el formulario

    foreach ($datos as $clave => $valor) {
        $datos[$clave] = $_POST[$clave];
    }

    // tengo que insertar el registro en la bbdd
    $query = "INSERT INTO mensajes(id,nombre,mensaje,fechaHora) 
    VALUES ('{$datos["id"]}',{$datos["nombre"]},'{$datos["mensaje"]},'{$datos["fechaHora"]}')";

    // ejecuto la consulta
    if ($conexion->query($query)) {
        $salida = "Mensaje insertado";
        // obtengo los datos del ultimo registro insertado
        $sql = "select * from mens where id={$conexion->insert_id}";

        $resultado = $conexion->query($sql);
        $salida .= gridView($resultado);
    } else {
        $salida = "Error insertando el registro" . $conexion->error;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Bienvenido <?= $datos["nombre"] ?></h1>
    <textarea name="mensaje" id="mensaje" cols="30" rows="10"></textarea>
    <button>Enviar</button>
</body>
</html>

