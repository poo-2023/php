<?php

function mostrar(...$valores){
        static $c=0;
        foreach ($valores as $valor) {
            echo "<br />$c-$valor<br />";
            $c++;
        }
} 


$fechaActual=time();

mostrar("Raro, raro", $fechaActual);
mostrar("d/m/y", $fechaActual);

mostrar(date("d/m/y"));
mostrar(date("j"), date("d"), date("D"));
mostrar(date("N"), date("F"), date("m"));
mostrar(date("Y"), date("Y"));

