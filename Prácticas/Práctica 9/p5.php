<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="ejercicio5Destino.php" method="GET">
       <div>
            <label for="nombre">
                Nombre:
            </label>
            <input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre" required>
       </div>
       <br>

       <div>
            <label for="apellidos">
                Apellidos:
            </label>
            <input type="text" name="apellidos" id="apellidos" placeholder="Introduce tus apellidos" required>
       </div>
       <br>

        <div>
            <label for="codigo">
                Código postal:
            </label>
            <input type="number" name="codigo" id="codigo" placeholder="Introduce tu código postal" required>
        </div>
        <br>


        <div>
            <label for="telefono">
                Teléfono:
            </label>
            <input type="text" id="telefono" name="telefono" placeholder="Introduce tu Teléfono" required>
        </div>
        <br>

        <div>
            <label for="email">
                Email:
            </label>
            <input type="email" name="email" id="correo" placeholder="Introduce tu correo" required>
        </div>
        <br>

        <div>
            <button>ENVIAR</button>
        </div>


    </form>
</body>
</html>