<?php

// el controlador solamente controla, no visualiza nada
require_once "controlador.php";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    // carga el formulario
    require "_form.php";
    // si se han mandado datos, enseña el resultado
    if($_POST){
        require "_resultados.php";
    }
    ?>

</body>
</html>