<?php

namespace clases\ejercicio2;
class Usuario{
    public int $idUsuario;
    public string $nombre;
    public string $email;
    public int $edad;
    public bool $activo;

    public function __construct(int $idUsuario, string $nombre, string $email, int $edad, bool $activo){
        $this->idUsuario = 0;
        $this->nombre = "";
        $this->email = "";
        $this->edad = 0;
        $this->activo = false;
    }
    public function mostrarInformacion(): string
    {
        return "ID: " . $this->idUsuario . ", Nombre: " . $this->nombre . ", Email: " . $this->email . ", Edad: " . $this->edad . ", Activo: " . $this->activo;
    }
    

}   