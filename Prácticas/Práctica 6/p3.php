<?php       

// creamos la variable número
$num=35;

// condicional que ejecuta una opcion si el resto de la variable es 0, y la opción alternativa si es impar
if($num%2==0){
    echo "El número {$num} es par";
}else{
    echo "El número {$num} es impar";
}