<?php
// tipifica los argumentos como numeros enteros
// tipifica el valor devuelto como numero entero
function suma(int $numero1, int $numero2): int
{
    return $numero1 + $numero2;
}


function resta(float $numero1, float $numero2): float
{
    return $numero1 - $numero2;
}

function producto(float $numero1, float $numero2): float
{
    return $numero1 * $numero2;
}

function cocniente(float $numero1, float $numero2): float
{
    return $numero1 / $numero2;
}

/*
funcion que recibe dos numeros y calcula
suma
resta
producto
cociente
los valores los debe devolver como un array asociativo donde los indices deben ser los nombres de las operaciones
*/

function operaciones(int $numero1, int $numero2): array
{

    $resultados = [
        "suma" =>  $numero1+$numero2,
        "resta" => $numero1-$numero2,
        "producto" => $numero1*$numero2,
        "cociente" => $numero1/$numero2,
    ];


    return $resultados;
}


/**
 * probar las funciones
 */

$a = mt_rand(1, 100);
$b = mt_rand(1, 100);
$resultados=operaciones($a,$b);

?>

<ul>
    <li>Numero 1 <?= $a ?></li>
    <li>Numero 2<?= $b ?></li>
</ul>

<?php
// calcular la suma con la funcion suma
// mostrar la suma en un div
$resultado=suma($a, $b);

?>
<div>
    Suma: <?= $resultado ?>
</div>

<?php
// calcular la resta con la funcion resta

$resultado=resta($a, $b);
// mostrar la resta en un div

?>

<div>
    Resta: <?= $resultado ?>
</div>

<?php
// calcular suma,resta,producto y cociente
// con la funcion operaciones

// mostrar los resultados en el siguiente listado
?>

<ul>
    <li>Suma: <?= $resultados["suma"]?> </li>
    <li>Resta: resultado <?= $resultados["resta"] ?></li>
    <li>Producto: <?= $resultados["producto"] ?></li>
    <li>Cociente: <?= $resultados["cociente"] ?></li>
</ul>