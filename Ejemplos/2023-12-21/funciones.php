<?php

$datos=[
    "red","rojo", "blue", "azul",
];

function mostrarArray(array $vector){
    for ($i=0; $i < count($vector)-1; $i++) { 
        echo "<th>{$vector[$i]}</th>";
        
    }
}

function mostrarArray2(array $datos): string
{
    $html = "<table>";
    foreach($datos as $indice => $valor){
        $html .= "<tr><th>$indice</th><td>$valor</td>";
    }

    $html .= "</table>";
    return $html;
}


function mostrarArray3(array $datos): string{
    $html= "<table>";
    array_walk($datos, function ($dato, $indice){
        echo "<tr><th>$indice</th><td>$dato</td>";
    });
    $html .= "</table>";
    return $html;
};

function mostrarArray4(array $datos): string
{
    $html="<table>";

    $datos1=array_map(function($valor)
    {
        return "<tr><td>$valor</td></tr>";
    }, $datos);

    $html .= implode("", $datos1);
    $html .= "</table>";

    return $html;
};