<?php   

//argunmentos por defecto siempre al final

function crearEtiqueta($contenido, $etiqueta = "div"){
    return "<{$etiqueta}>{$contenido}</{$etiqueta}>";
}

echo crearEtiqueta("Hola mundo");

echo crearEtiqueta("Hola mundo", "p");

