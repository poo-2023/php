<?php

$parametros = require_once "parametros.php";

require_once "funciones.php";

// conexion a base de datos
$conexion = @new mysqli(
    $parametros["servidor"],
    $parametros["usuario"],
    $parametros["password"],
    $parametros["nombreBd"]
);

// compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}

$sql="DELETE FROM empleados WHERE id=1";

if ($resultados = $conexion->query($sql)) {
    $salida = gridView($resultados);
} else {
    $salida = "Error al ejecutar la consulta: " . $conexion->error;
}
