<?php
// definiendo el autoload

use clases\Aplicacion;
use clases\Pagina;
use clases\Header;
use clases\Modelo;


spl_autoload_register(function ($clase) {
    include $clase . '.php';
});

$aplicacion = new Aplicacion();
$favoritos = new Modelo($aplicacion->db);
$favoritos->query("select * from favoritos where categorias = 'buscador'");


$cabecera = Header::ejecutar([
    'titulo' => 'Buscadores',
    'subtitulo' => "",
    'salida' => 'Paginas de busqueda'
]);

Pagina::comenzar();
?>
    <?= $favoritos->gridViewBotones(); ?>
<?php
Pagina::terminar([
    "titulo" => "inicio",
    "cabecera" => $cabecera,
    "pie" => "Creado por: " . $aplicacion->configuraciones['autor']
]);
