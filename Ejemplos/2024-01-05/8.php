<?php

function crearEtiqueta($contenido, $etiqueta,...$atributos)
{
    $resultado="<{$etiqueta} ";
    foreach ($atributos as $atributo){
        $resultado .= "<{$atributo} ";
    }
    $resultado .= ">{$contenido}</{$etiqueta}>";
    return $resultado;
}

echo crearEtiqueta("Hola", "div", "class=>'caja'", "id='caja1'");

