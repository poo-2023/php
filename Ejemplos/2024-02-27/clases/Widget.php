<?php

namespace clases;

class Widget
{
    public static function menu()
    {
        return "menu";
    }

    public static function comenzar()
    {
        // controla el flujo de salida
        // para que no se muestre el HTML
        // en el navegador
        ob_start();
    }

    public static function ejecutar(array $parametros = [], string $layout = 'layout1'): string
    {
        extract($parametros);
        ob_start();
        require "layouts/" . $layout . ".php";
        return ob_get_clean();
    }


    public static function terminar( array $parametros, string $layout = 'layout1'): void
    {
        // termina el flujo de salida
        // para que no se muestre el HTML
        // en el navegador
        $contenido = ob_get_clean();
        extract ($parametros);
        require "layouts/" . $layout . ".php";
    }

}