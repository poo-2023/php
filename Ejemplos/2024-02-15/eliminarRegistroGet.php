<?php


$parametros = require_once "parametros.php";

require_once "funciones.php";

// conexion a base de datos
$conexion = @new mysqli(
    $parametros["servidor"],
    $parametros["usuario"],
    $parametros["password"],
    $parametros["nombreBd"]
);

// compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}

// el registro a borrar llega por get
$sql="DELETE FROM empleados WHERE id=" . $_GET["id"];

if ($conexion->query($sql)) {
    $salida="Registro eliminado correctamente";
    $slaida .="<br><a href='listarRegistros.php'>Vlver al listado</a>";
} else {
    $salida = "Error al ejecutar la consulta: " . $conexion->error;
}
