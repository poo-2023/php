<?php



$ficheroSubido=false;
$mensaje1="";
$mensaje2="";

if($_FILES){  
    
    $documento=$_FILES["pdf"];
    $imagen=$_FILES["img"];

    $mipdf=$_FILES["pdf"]["name"];
    $mifoto=$_FILES["img"]["name"];
    
    $rutaDocumento="./pdfs/" . $mipdf;
    $rutaImagen="./imgs/" . $mifoto;
    
    $documentoSubido=move_uploaded_file($documento["tmp_name"], $rutaDocumento);
    $imgSubido=move_uploaded_file($imagen["tmp_name"], $rutaImagen);

    if($documentoSubido){
        $mensaje1="Se ha subido el pdf";
    }else{
        $mensaje1="No se ha podido subir el pdf";
    }

    if($imgSubido){
        $mensaje2="Se ha subido la imagen";
    }else{
        $mensaje2="No se ha podido subir la imagen";
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <form method="post" enctype="multipart/form-data">
        <label for="img">Imagen</label>
        <input type="file" name="img">
        <br>
        <label for="pdf">PDF</label>
        <input type="file" name="pdf">
        <br>
        <button>Enviar</button>
    </form>

    <?= $mensaje1 ?>
    <br>
    <?= $mensaje2 ?>
    
</body>
</html>