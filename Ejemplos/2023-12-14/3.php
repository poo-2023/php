<?php

$numeros=[1,2,1,2,3,1,2];

// crear una variable de tipo string con los numeros separados por comas

$csv="";

//1º usar un foreach y concatenar los números
//2º usar la función implode

for ($i=0; $i < count($numeros); $i++) { 
    
    if($i<count($numeros)-1){
        $csv=$csv.$numeros[$i].", ";
    }else{
        $csv=$csv.$numeros[$i];
    }

}

echo $csv;