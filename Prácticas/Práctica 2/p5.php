<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
<?php
    
    $ancho=200;
    $alto=100;

    // opción 1
    echo "<div style=\"background-color: black; width:{$ancho}px; height:{$alto}px\"></div>";

    // opción 2
    echo '<div style="background-color: black; width:' . $ancho . 'px;height:' . $alto . 'px"></div>';
?>



</body>
</html>
