<?php

//si no vienes del fomulario, carga el formulario vacío
if(!$_POST){
    header("Location: 1-destino.php");
}

// no se recibe con get porque el formulario es post
// var_dump($_GET);

// tendría que leerlo con post
// var_dump($_POST);

// no es recomendable, aunque recoge tanto get como post
// var_dump($_REQUEST);

$operacion=$_POST["operacion"] ?: "";
$numero1=$_POST["numero1"] ?: 0;
$numero2=$_POST["numero2"] ?: 0;

switch ($operacion) {
    case 'sumar':
        $resultado=$numero1+$numero2;
        break;
    case 'restar':
        $resultado=$numero1-$numero2;
        break;
    default:
        $resultado=0;
        break;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?= $resultado ?>
</body>
</html>