<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>

        /*
        selector por clase
        */

        .caja{
            margin:10px auto;
            width:300px;
            background-color:black;
            color:white;
        }
        
        .centrado{
            text-align:center;
        }

        /*  
        selector por id
        */

        #negrita{
            font-weight: bold;
        }

        /*
        selector por etiqueta  
        */
        
        div{
            border:1px solid red;
        }

    </style>
</head>
<body>
    <div id="negrita" class="caja">
        Hola mundo
    </div>
    <div class="caja centrado">
        Adiós mundo
    </div>
</body>
</html>