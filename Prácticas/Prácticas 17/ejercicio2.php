<?php

use \clases\ejercicio2\Lavadora;

spl_autoload_register(function ($clase) {
    include $clase . '.php';
});

$objeto=new Lavadora("Balay", "Lavadora", 220, 200, true);

echo $objeto->toString();
echo $objeto->getCosteConsumo(12, 0.5);