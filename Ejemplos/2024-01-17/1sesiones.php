<?php
session_name("form");
session_start();
// crear un formulario que me permita introducir un número 
// almacenar el número en una variable de sesión

if(isset($_POST["numero"])){
    $_SESSION["dato"]=$_POST["numero"];
}else{
    $_SESSION["dato"]=0;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="post">
        <div>
            <label for="numero" name="numero">Numero</label>
            <input type="number" name="numero" id="numero">
        </div>
        <div>
            <button>Enviar</button>
        </div>
    </form>
</body>
</html>