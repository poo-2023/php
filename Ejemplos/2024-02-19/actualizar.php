<?php
session_start();

require_once "funciones.php";

// cargo los parametros de aplicacion
$parametros = require_once("parametros.php");

// desactivar errores
controlErrores();


// creo un array con los elementos
// que quiero que tenga el menu
$elementosMenu = [
    "Inicio" => "index.php",
    "Insertar" => "insertar.php"
];

// preparo el menu
$menu = menu($elementosMenu);

// conexion a base de datos
$conexion = @new mysqli(
    $parametros["bd"]["servidor"],
    $parametros["bd"]["usuario"],
    $parametros["bd"]["password"],
    $parametros["bd"]["nombreBd"]
);

// compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}

// inicializo la salida de la vista
$salida = "";

// comprobar si vengo de pulsar sobre el boton editar
// del gridview y por lo tanto me llega el id por GET
if (isset($_GET["id"])) {

    $_SESSION["id"] = $_GET["id"];
    // preparo la consulta para obtener los datos del libro 
    // a modificar
    $sql = "select * from libros where id={$_GET["id"]}";
    $resultado = $conexion->query($sql);
    $datos = $resultado->fetch_assoc();
}

// comprobar si he pulsado de actualizar en el formulario
// despues de que el usuario haya cambiado los datos
if ($_POST) {
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Editar</h1>

    <?php
    require "_form.php";
    ?>

    <?= $salida ?>

</body>

</html>
