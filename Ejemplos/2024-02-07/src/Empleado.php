<?php

namespace src;

class Empleado extends Persona{

    private ?float $sueldo;

    // sobreescribo el método mostrar de la clase Persona

    public function mostrar():string
    {
        $salida="<ul>";
        $salida.="<li>Nombre: " . $this->getNombre() . "</li>";
        $salida.="<li>Edad: " . $this->getEdad() . "</li>";
        $salida.="<li>Sueldo: " . $this->sueldo . "</li>";
        $salida.="</ul>";
        return $salida;
    }

    public function __construct()
    {
        parent :: __construct();
        $this->sueldo=null;
        // en el constructor indico que campos quiero que se asignen automáticamente
        $this->propiedadesAsignacionMasiva=["nombre","edad"];
    }

    public function calcularSuledo(int $horas): void
    {
        $this->sueldo=$horas * 10;
    }

    // sobreescribo el método asignar

    public function asignar(array $datos): self
    {
        parent::asignar($datos);
        // asigno campos que no están en asignación masiva
        $this->calcularSuledo($datos["horas"]);
        return $this;
    }

}