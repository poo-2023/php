<?php
// arranco sesiones
session_start();

// cargo las funciones
require_once "funciones.php";

// control de errores
controlErrores();

// inicializo las variables
$salida = "";
$aviso = "";
$parametros = require_once "parametros.php";

// compruebo si usuario esta logueado
if (!isset($_SESSION["nombre"])) {
    // si no lo esta, redirijo a login
    header("Location: index.php");
} else {
    // si esta logueado
    $menu = menu([
        "Inicio" => "index.php",
        "Mensaje" => "mensaje.php",
        "Salir" => "salir.php",
    ]);

    // me conecto a la base de datos
    $conexion = new mysqli(
        $parametros["bd"]["servidor"],
        $parametros["bd"]["usuario"],
        $parametros["bd"]["password"],
        $parametros["bd"]["nombreBd"]
    );

    // compruebo si la conexion es correcta
    if ($conexion->connect_error) {
        die("Error de conexión: " . $conexion->connect_error);
    }

    // comprobar si he pulsado el boton de añadir mensaje (Enviar)
    if ($_POST) {
        // leer los datos del formulario
        $datos["mensaje"] = $_POST["mensaje"];

        // creo la fecha
        $fecha = date("Y-m-d H:i:s");

        // preparo la consulta
        $sql = "INSERT INTO mensaje (nombre,mensaje,fecha) 
        VALUES ('$_SESSION[nombre]','{$datos["mensaje"]}','$fecha')";
        // ejecuto la consulta
        $aviso = '<div class="row alert alert-warning">';
        if ($conexion->query($sql)) {
            $aviso .= "Mensaje añadido";
        } else {
            $aviso .= "Error al insertar";
        }
        $aviso .= "</div>";
    }

    // preparar el texto de la consulta
    $sql = "SELECT * FROM mensaje";

    // obtengo el mysqli_result
    $resultados = $conexion->query($sql);

    $salida = gridViewBotones($resultados, [
        "borrar" => "borrar.php"
    ]);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="css/home.css">
</head>

<body>
    <div>
        <?= $menu ?>
    </div>
    <div class="container">
        <div class="row bg-dark text-light p-1 rounded my-2">
            <h1>Mensajes</h1>
        </div>

        <?= $aviso ?>

        <div class="row">
            <?php
            require "_form.php";
            ?>
        </div>

        <div class="row">
            <?= $salida ?>
        </div>

    </div>

    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>

</html>
