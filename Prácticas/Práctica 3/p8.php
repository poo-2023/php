<?php   

$datos=[
    [
    "nombre"=> "Eva",
    "edad"=> 50,
    ],[
    "nombre" =>"Jose",  
    "edad" =>40,
    "peso" => 80,
    ]
];

// introducir a "lorena" de 80 años y con una altura de 175
// realizarlo directamente

$datos[]=["nombre"=>"Lorena", "edad"=>80, "altura"=>175];

// introducir a "luis" de 20 años y con un peso de 90 y a "oscar" de 23 años
// realizarlo mediante la función push

array_push($datos, ["nombre"=>"Luis", "peso"=>20, ],["nombre"=> "Oscar", "edad"=>23]);

var_dump($datos);