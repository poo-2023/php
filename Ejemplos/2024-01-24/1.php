<?php

spl_autoload_register(function($clase){
    require "clases/" . $clase . ".php";
});

$humano=new Humano("marcos", "hombre");
$oficio=new Oficio("pintor", 14, 40);
$trabajan=new Trabajan($humano, $oficio);

var_dump($humano);
var_dump($oficio);
var_dump($trabajan);