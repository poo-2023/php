<?php

// inicialización de variables
$a=13;
$b=20;
$resultado=0;


// array con todas las opciones de las operaciones
$operaciones=[
    "suma",
    "resta",
    "multiplicacion",
    "division",
    "raiz",
    "potencia",
];

// variable que guarda la variable seleccionada
$opcion="resta";

// switch que opera y imprime resultados
switch ($opcion) {

    case "suma":
        $resultado=$a+$b;
        echo $resultado;
        break;
    case "resta":
        $resultado=$a-$b;
        echo $resultado;
        break;
    case "multiplicacion":
        $resultado=$a*$b;
        echo $resultado;
        break;
    case "division":
        $resultado=$a/$b;
        echo $resultado;
        break;
    case "raiz":
        $resultado=sqrt($a);
        echo $resultado;
        break;
    case "potencia":
        $resultado=pow($a,$b);
        echo $resultado;
        break;
}
