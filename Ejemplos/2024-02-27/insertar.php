<?php

// cargando la clase en el espacio de nombres actual
use clases\Aplicacion;
use clases\GridView;
use clases\Header;
use clases\Modelo;
use clases\Pagina;


// definiendo el autoload
spl_autoload_register(function ($clase) {
    include $clase . '.php';
});
// inicializo mensaje
$mensaje = "";

// instanciando la clase
$aplicacion = new Aplicacion();
$favoritos = new Modelo($aplicacion->db);

// le indico que campos voy a insertar
// inicializa los datos
$favoritos->setCampos(["url", "titulo", "descripcion", "categorias"]);
// le indico al modelo en donde tiene que insertar el registro
$favoritos->tabla = "favoritos";

// compruebo si me llegan datos del post

if ($_POST) {
    $favoritos->setDatos($_POST)->save();
    $mensaje = "<div class='alert alert-success'>El registro se ha insertado correctamente</div>";
} 

$cabecera = Header::ejecutar([
    "titulo" => "Pagina de insercion",
    "subtitulo" => $aplicacion->configuraciones['autor'],
    "salida" => "Aplicacion para mostrar paginas interesantes"
]);

Pagina::comenzar();
?>

<?php
// al formulario le paso los datos y la accion a mostrar en el boton
if(!$_POST){
    $datos = $favoritos->datos;
    $accion = "insertar";
    require "_form.php";
}else{
    echo $mensaje;
}
?>

<?php
Pagina::terminar([
    "titulo" => "inicio",
    "cabecera" => $cabecera,
    "pie" => "Creado por: " . $aplicacion->configuraciones['autor']
]);

