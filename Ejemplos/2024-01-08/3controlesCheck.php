<?php

// comprobamos si se ha pulsado el botón de enviar
// si se ha pulsado, imprime los datos enviados

$resultado="";

if($_POST){
    $resultado=implode(",", $_POST["color"]);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
<form method="post" >
    <input type="checkbox" name="color[]" value="rojo" id="rojo">
    <label for="rojo">Rojo</label>
    <input type="checkbox" name="color[]" value="azul" id="azul">
    <label for="azul">Azul</label>
    <input type="checkbox" name="color[]" value="verde" id="verde">
    <label for="verde">Verde</label>
    <button type="submit">Enviar</button>
</form>

<?= $resultado ?>

</body>
</html>