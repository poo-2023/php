<?php

spl_autoload_register(function($clase){
    require "clases/" . $clase . ".php";
});

$gato1=new Gato();
$gato1->peso=6;

$gato2=new Gato("jorge", "negro", "no");

var_dump($gato1);
var_dump($gato2);