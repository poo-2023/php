<?php

// cargo funciones
require_once "funciones.php";

// cargo los parámetros de aplicacion
$parametros = require_once("parametros.php");

// desactivar errores
controlErrores();

// conexión a base de datos
$conexion = @new mysqli(
    $parametros["bd"]["servidor"],
    $parametros["bd"]["usuario"],
    $parametros["bd"]["password"],
    $parametros["bd"]["nombreBd"]
);

// compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}

// array con los datos de la consulta
$datos = [
    "id" => "",
    "nombre" => "",
    "mensaje" => "",
    "fechaHora" => "",
];

// preparo la salida
$salida = "";

// compruebo si había datos en usuario
if($_POST["usuario"]){
    $datos["nombre"] = $_POST["usuario"];
}else{
    $salida = "Introduce un usuario";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<form method="post" 
<?php
    if($_POST["usuario"]){
        echo "action='mensajes.php'";
    }
?>
    >
    <label for="usuario" name="usuario">Usuario: </label>
    <input type="text" name="usuario" id="usuario">
    <button type="submit" id="login">Login</button>

    <?= $salida ?>

</form>

</body>
</html>