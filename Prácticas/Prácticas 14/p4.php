<?php

function mostrar(...$valores){
    foreach($valores as $valor){
        echo "<br />$valor<br />";
    }
}

setlocale(LC_ALL, "spanish");
mostrar(strftime("%A/%B"));
mostrar(strftime("%A", mktime(0,0,0,1,1,1901)));

