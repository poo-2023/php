<?php

// Creando variables
$nombre="Fran";
$poblacion="Santander";

?>

<!-- mostrar en 2 divs usando impresión corta -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <div>
        <?= $nombre ?>
    </div>
    
    <div>
        <?= $poblacion ?>
    </div>

</body>
</html>