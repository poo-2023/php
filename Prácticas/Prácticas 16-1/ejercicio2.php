<?php
use clases\ejercicio2\Usuario;

spl_autoload_register(function ($clase) {
    include $clase . '.php';
});


$usuario1=new Usuario(1, "Juan", "j@j.com", 20, true);
echo $usuario1->mostrarInformacion();