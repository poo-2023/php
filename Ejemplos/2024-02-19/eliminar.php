<?php
session_start();

require_once "funciones.php";

// cargo los parametros de aplicacion
$parametros = require_once("parametros.php");
$tabla = "libros";

// desactivar errores
controlErrores();

// creo un array con los elementos
// que quiero que tenga el menu
$elementosMenu = [
    "Inicio" => "index.php",
    "Insertar" => "insertar.php"
];

// preparo el menu
$menu = menu($elementosMenu);

// conexion a base de datos
$conexion = @new mysqli(
    $parametros["bd"]["servidor"],
    $parametros["bd"]["usuario"],
    $parametros["bd"]["password"],
    $parametros["bd"]["nombreBd"]
);

// compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}

// inicializo la salida de la vista
$salida = "";

// comprobar si vengo de pulsar sobre el boton eliminar
// del gridview y por lo tanto me llega el id por GET
if (isset($_GET["id"])) {

    // preparo la consulta para eliminar el libro del que me llega la id

    $sql = "delete from {$tabla} where id={$_GET["id"]}";
    if ($resultado = $conexion->query($sql)) {
        $salida = "Registro eliminado correctamente";
    } else {
        $salida = "Error al eliminar el registro" . $conexion->error;
    }
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Editar</h1>

    <?= $menu ?>

    <?= $salida ?>

</body>

</html>
