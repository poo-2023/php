<?php

// esta función es similar a la del ejercicion anterior
// cambia en pedir un array por referencia, que modifica lo que se hace al argumento
// no tiene return
function ejercicio($minimo, $maximo, $numero, &$vector){

    $vector=[];

    for ($i=0; $i < $numero; $i++ ) { 
        $vector[$i]=mt_rand($minimo, $maximo);
    }

}