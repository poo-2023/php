<?php

/**
* funcion para mostrar 
* los datos de un array bidimensional 
* donde en la primera dimensioón tengo los
* registros y en la segunda dimensión los campos
* mostramos los resultados 
* la función se llamrá mostrarDatos que recibe como argumento el array dimesional
*/


function mostrarDatos(array $vector): string{
    $html= "<table>";
    //registros
    foreach($vector as $registro){
        //campos
        $html .= "<tr>";
        foreach($registro as $dato){
            $html .= "<td>{$dato}</td>";
        }
        $html .= "</tr>";
    }

    $html .= "</table>";
    return $html;
};


function mostrarDatosV1(array $datos): string{
    
    $html= "<table>";
    //registros
    foreach($datos as $registro){
        //campos
        $html .= "<tr>";
        array_walk($registro, function($registro)use (&$html){
            

        });
        $html .= "</tr>";
    }

    $html .= "</table>";
    return $html;
}

?>

