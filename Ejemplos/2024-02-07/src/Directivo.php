<?php

namespace src;

class Directivo extends Empleado{
    private ?string $categoria;

    // sobreescribir mostrar 

    public function mostrar(): string
    {
        $salida="<ul>";
        $salida.="<li>Nombre: " . $this->getNombre() . "</li>";
        $salida.="<li>Edad: " . $this->getEdad() . "</li>";
        $salida.="<li>Categoria: " . $this->categoria . "</li>";
        $salida.="</ul>";

        return $salida;
    }

    public function __toString(): string
    {
        return $this->mostrar();
    }
    // getters y setters

    public function getCategoria(): string
    {
        return $this->categoria;
    }

    public function setCategoria(string $categoria): void
    {
        $this->categoria = $categoria;
    }

    public function __construct()
    {
        parent :: __construct();
        $this->categoria=null;
        $this->propiedadesAsignacionMasiva=["nombre","edad","categoria"];
    }
    
    public function asignar(array $datos):  self
    {
        $this->setNombre($datos["nombre"]);
        $this->setEdad($datos["edad"]);
        $this->categoria=($datos["categoria"]);
        return $this;
    }
}