<?php

$parametros = require_once "parametros.php";

require_once "funciones.php";

// he inicializado la variable que muestra 
// el mensaje de si ha sido correcta 
// la insercion o no
$salida = "";


$datos= [
    "id" => "",
    "nombre" => "",
    "apellidos" => "",
    "edad" => "",
    "poblacion" => "",
    "codigoPostal" => "",
    "fechaNacimiento" => ""
];
// desactivar errores
controlErrores();

// comprobar si he pulsado el boton de insertar
if ($_POST) {
    $datos["nombre"] = $_POST["nombre"];
    $datos["apellidos"] = $_POST["apellidos"];
    $datos["edad"] = $_POST["edad"];
    $datos["poblacion"] = $_POST["poblacion"];
    $datos["codigoPostal"] = $_POST["codigoPostal"];
    $datos["fechaNacimiento"] = $_POST["fechaNacimiento"];

    // conexion a base de datos
    $conexion = @new mysqli(
        $parametros["servidor"],
        $parametros["usuario"],
        $parametros["password"],
        $parametros["nombreBd"]
    );

    // compruebo si la conexion es correcta
    if ($conexion->connect_error) {
        die("Error de conexión: " . $conexion->connect_error);
    }

    $sql = "INSERT INTO empleados 
  (nombre, apellidos, edad, poblacion, codigoPostal, fechaNacimiento) VALUES 
  (
    '{$datos["nombre"]}',
    '{$datos["apellidos"]}',
    {$datos["edad"]},
    '{$datos["poblacion"]}',
    '{$datos["codigoPostal"]}',
    '{$datos["fechaNacimiento"]}'

  )";

    if ($conexion->query($sql)) {
        $salida = "Registro insertado correctamente";
        $salida .= "<br><a href='listarRegistrosUpdateDelete.php'>Ver todos los registros</a>";
    } else {
        $salida = "Error al insertar el registro: " . $conexion->error;
    }

    $conexion->close();
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $parametros["nombreAplicacion"] ?></title>
</head>

<body>
    <h1><?= $parametros["nombreAplicacion"] ?></h1>
    <div>
        <?php
        require "_form.php";
        ?>
    </div>
    <div>
        <?= $salida ?>
    </div>

</body>

</html>
