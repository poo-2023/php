<?php

$datos=[
    [
        "id"=>1,
        "nombre"=>"Juan",
        "apellidos"=>"Peña",
    ],
    [
        "id"=>2,
        "nombre"=>"Pedro",
        "apellidos"=>"Peña",
    ],
    [
        "id"=>3,
        "nombre"=>"Ana",
        "apellidos"=>"Peña",
    ]
];

//mostrar el array datos entero en una tabla

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <table border=2>
        <thead>
            <th>Id</th>
            <th>Nombre</th>
            <th>Apellidos</th>
        </thead>

<?php

    for ($i=0; $i < count($datos); $i++) { 
        echo "<tr>";
        foreach ($datos[$i] as $key => $persona) {
            echo "<td>  {$persona}  </td>";
        };
        echo "</td>";
    }

?>
    </table>

</body>
</html>